import {
    APP_INITIALIZER,
    NgModule
} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { PluginTerraBasicComponent } from './mail-in-one.component';
import { CampaignComponent } from './campaign/campaign.component';
import { TerraComponentsModule } from '@plentymarkets/terra-components/app/terra-components.module';
import { HttpModule } from '@angular/http';
import {
    L10nLoader,
    TranslationModule
} from 'angular-l10n';
import { FormsModule } from '@angular/forms';
import { PluginTerraBasicConfig } from './mail-in-one.config';
import { SettingsComponent } from './settings/settings.component';
import { SettingsService } from './settings/service/settings.service';
import { CampaignService } from "./campaign/service/campaign.service";
import { ContactFilterService } from "./contactfilter/service/contactFilter.service";
import { ContactFilterComponent } from "./contactfilter/contactfilter.component";
import { TargetGroupService } from "./targetGroup/service/targetGroup.service";
import { TargetGroupComponent } from "./targetGroup/targetGroup.component";

import { getAllSyncStatusService } from "./getAllSyncStatus/service/getAllSyncStatus.service";
import { GetAllSyncStatusComponent } from "./getAllSyncStatus/getAllSyncStatus.component";

import { ContactSyncComponent } from "./contactSync/contactSync.component";
import { ContactSyncService } from "./contactSync/service/contactSync.service";
import { EventComponent } from "./event/event.component";
import { EventService } from "./event/service/event.service";
import { BasketEventComponent } from "./basketEvent/basketEvent.component";
import { BasketEventService } from "./basketEvent/service/basketEvent.service";
import { l10nConfig } from "@plentymarkets/terra-components/app/translation/l10n.config";
import { getMioDoiSubscriberStatusService } from './getMioDoiSubscriberStatus/service/getMioDoiSubscriberStatus.service';
import { GetMioDoiSubscriberStatusComponent } from './getMioDoiSubscriberStatus/getMioDoiSubscriberStatus.component';
import { BlacklistComponent } from './blacklist/blacklist.component';
import { BlacklistService } from './blacklist/service/blacklist.service';
import { GetMioEmailStatusComponent } from './getMioEmailStatus/getMioEmailStatus.component';
import { GetMioEmailStatusService } from './getMioEmailStatus/service/getMioEmailStatus.service';
@NgModule({
    imports: [
        BrowserModule,
        HttpModule,
        FormsModule,
        TranslationModule.forRoot(l10nConfig),
        TerraComponentsModule.forRoot()
    ],
    declarations: [
        PluginTerraBasicComponent,
        CampaignComponent,
        SettingsComponent,
        ContactFilterComponent,
        TargetGroupComponent,
        ContactSyncComponent,
        EventComponent,
        BasketEventComponent,
        GetAllSyncStatusComponent,
        GetMioDoiSubscriberStatusComponent,
        BlacklistComponent,
        GetMioEmailStatusComponent,
    ],
    providers: [PluginTerraBasicConfig,
        SettingsService,
        CampaignService,
        ContactFilterService,
        TargetGroupService,
        ContactSyncService,
        EventService,
        getAllSyncStatusService,
        BasketEventService,
        getMioDoiSubscriberStatusService,
        BlacklistService,
        GetMioEmailStatusService,
        {
            provide: APP_INITIALIZER,
            useFactory: initL10n,
            deps: [L10nLoader],
            multi: true
        }
    ],
    bootstrap: [
        PluginTerraBasicComponent
    ]
})

export class PluginTerraBasicModule {
    constructor(public l10nLoader: L10nLoader) {
        this.l10nLoader.load();
    }
}
function initL10n(l10nLoader: L10nLoader): Function {
    return (): Promise<void> => l10nLoader.load();
}
