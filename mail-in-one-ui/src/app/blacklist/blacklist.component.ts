import {
    Component,
    Input,
    OnInit
} from '@angular/core';
import {
    Translation,
    TranslationService
} from 'angular-l10n';
//
import { BlacklistService } from "./service/blacklist.service";
import { BlacklistInterface } from './data/blacklist.interface';
import { TerraSimpleTableHeaderCellInterface, TerraSimpleTableRowInterface, TerraToggleComponent } from "@plentymarkets/terra-components";
import { PluginTerraBasicConfig } from '../mail-in-one.config';
import { SelectBoxValueInterface } from "../event/data/select-box.interface";
import { ResponseOptions } from '@angular/http';


@Component({
    selector: 'blacklist',
    template: require('./blacklist.component.html'),
    styles: [require('./blacklist.component.scss')],
})

export class BlacklistComponent implements OnInit {

    @Input() myTitle: string;
    // @Output() toggled :<EventEmitter>;  ????
    private service: BlacklistService;
    private _isToggled: boolean = true;
    private isLoading: boolean = true;
    private targetGroup: any;
    private headerList: Array<TerraSimpleTableHeaderCellInterface> = [];
    private rowList: Array<TerraSimpleTableRowInterface<any>> = [];
    private standardBlacklist: string = null;
    private hasBlacklist: boolean = false;
    private hasBlacklistEntries: boolean = false;
    private _blacklistSelectBox: Array<SelectBoxValueInterface> = [];
    private _blacklists: Array<any> = [];
    private _blacklistEntries: Array<any> = [];
    private _blacklistId: any;


    private ui_new_bl_email: string;
    private ui_add_to_blacklist_id: string;

    private _emailTAoBlock: string;

    constructor(
        public translation: TranslationService,
        private _blacklistService: BlacklistService,
        private _pluginTerraBasicConfig: PluginTerraBasicConfig
    ) {
        this.service = _blacklistService;
        this.ui_add_to_blacklist_id = '';
        this.ui_new_bl_email = '';
    }

    ngOnInit() {

        this.getMioPluginOptionStandardBlacklist();
        console.log('ngInit standardBlacklist=' + this.standardBlacklist);//TODO: remove
        console.debug(this);

        // Table headerList
        this.headerList = [
            {
                width: '20px',
                caption: 'geblockte Email'
            },
            {
                width: '20px',
                caption: 'Grund Blockierung'
            },
            {
                width: '20px',
                caption: 'Zeitpunkt Blockierung'
            }
        ];

        // Table rowList. Uses route !!! 'mailInOne/getMioBlockedContacts/' !!!
        this._blacklistService.getMioBlacklistEntriesFlatList().subscribe(
            response => {
                for (let bl_entry of response) {

                    console.log('bl_entry = ' + JSON.stringify(bl_entry));

                    if (bl_entry['email'].length > 0) {
                        this.hasBlacklistEntries = true;
                    }

                    this.rowList.push(
                        {
                            cellList: [
                                {
                                    caption: bl_entry["email"]
                                },
                                {
                                    caption: bl_entry["blocking_reason"]
                                },
                                {
                                    caption: bl_entry["blocking_timestamp"]
                                }
                            ]
                        }
                    )
                }
            },
            (error) => {
                this.hasBlacklistEntries = false;
                let err_stat = (error.statusText != '' && error.statusText != undefined) ? error.statusText : '';
                this._pluginTerraBasicConfig.pluginTerraBasicComponent.callStatusEvent('Keine Blacklist-Einträge vorhanden' + ': ' + err_stat,
                    'danger');
                this.isLoading = false;
            }
        );

        this.logBlacklistBooleans();
    }

    // TODO: remove
    private logBlacklistBooleans() {
        return;
    }

    private isEmpty(value: any) {
        switch (typeof (value)) {
            case "number": return (value.toString().length === 0);
            case "string": return (value.length === 0);
            case "boolean": return false;
            case "undefined": return true;
            case "object": return !value ? true : false;
            default: return !value ? true : false
        }
    }


    private isEmail(email: string) {
        let validemail = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
        return validemail.test(String(email).toLowerCase());
    }


    public addEmailToBlacklistInMio() {
        this.getMioPluginOptionStandardBlacklist();
        // this.setBlacklistBooleans(); // TODO: remove

        // TODO: remove
        console.log("ADD TO BLACKLIST #" + this.standardBlacklist + "  EMAIL=" + this.ui_new_bl_email);

        // if (this.isEmpty(this.ui_add_to_blacklist_id || this.isEmpty(this.ui_new_bl_email))) {
        if (this.standardBlacklist === null) {
            this._pluginTerraBasicConfig.pluginTerraBasicComponent.callStatusEvent('Bitte Mail-In-One Standard-Blacklist in Kontoeinstellungen setzen',
                'danger');
            return false;
        }

        if (this.isEmpty(this.ui_new_bl_email)) {
            this._pluginTerraBasicConfig.pluginTerraBasicComponent.callStatusEvent('Bitte geben Sie eine Email-Adresse an',
                'danger');
            return false;
        }

        /*  do not check for vaild email here, since wildcard-entries shall be allowed

        if (!this.isEmail(this.ui_new_bl_email)) {
            this._pluginTerraBasicConfig.pluginTerraBasicComponent.callStatusEvent('Bitte korrekte Emailadresse angeben',
                'danger');
            return false;
        }

        */
        let blacklistid = this.standardBlacklist;

        let itemApi: BlacklistInterface = {
            id: blacklistid.toString(),
            entries: this.ui_new_bl_email
        };

        // TODO: may be remove or leave it: decode in console with atob( string )  to check settings
        console.log('blacklist-entry to save = ' + btoa(JSON.stringify(itemApi)));

        let dataApi = {
            settings: itemApi
        };

        console.log('dataApi = ' + dataApi);

        this.service.addToMioBlacklist(dataApi).subscribe(
            response => {
                this._pluginTerraBasicConfig.pluginTerraBasicComponent.callStatusEvent('Blacklist-Eintrag hinzugefügt.',
                    'success');
                this.isLoading = true;
            },
            (error) => {
                this._pluginTerraBasicConfig.pluginTerraBasicComponent.callStatusEvent('Der Eintrag konnte nicht  gespeichert werden.' + ': ' + error.statusText,
                    'danger');
                this.isLoading = false;
            }
        );

    }


    public getMioPluginOptionStandardBlacklist() {
        this._blacklistService.getMioPluginOptionStandardBlacklist().subscribe(
            response => {
                this.standardBlacklist = response;
                if (this.standardBlacklist.toString().length > 0) {
                    this.hasBlacklist = true;
                }
                console.log("response bl = " + this.standardBlacklist); // TODO: remove
            }
        )
        /*
        if (this.standardBlacklist === null || this.standardBlacklist == '0') {
            this.standardBlacklist = null;
        }
        */
        console.log("Standard-Blacklist = " + this.standardBlacklist);
    }



    public myToggle() {
        this.hasBlacklist = !this.hasBlacklist;
        this.hasBlacklistEntries = !this.hasBlacklistEntries;
        return !this._isToggled;

    };
}// class
