import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import {
    TerraBaseService,
    TerraLoadingSpinnerService
} from '@plentymarkets/terra-components';
import { Observable } from 'rxjs';
import { BlacklistInterface } from '../data/blacklist.interface';


@Injectable()

export class BlacklistService extends TerraBaseService {

    constructor(private _loadingSpinnerService: TerraLoadingSpinnerService,
        private _http: Http) {
        super(_loadingSpinnerService, _http, '/rest/');
        console.log(this.url);
    }


    public getMioBlacklistEntriesFlatList(): Observable<any> {

        this.setAuthorization();
        let url: string;

        //url = this.url + 'mailInOne/getMioBlacklistEntriesFlatList/';
        url = this.url + 'mailInOne/getMioBlockedContacts/'; // Mischung Blacklist und blocked Contacts nach Anforderung WEB-2446

        return this.mapRequest
            (
                this.http.get(url, {
                    headers: this.headers,
                    body: ''
                })
            );
    }

    // get data from options table WEB-2446
    public getMioPluginOptionStandardBlacklist(): Observable<any> {
        this.setAuthorization();

        let url: string;

        url = this.url + 'mailInOne/getMioPluginOptionStandardBlacklist/';

        return this.mapRequest(
            this.http.get(url, {
                headers: this.headers,
                body: ''
            })
        );
    }


    public getMioBlacklists(): Observable<any> {

        this.setAuthorization();
        let url: string;

        url = this.url + 'mailInOne/getMioBlacklists/';

        return this.mapRequest
            (
                this.http.get(url, {
                    headers: this.headers,
                    body: ''
                })
            );
    }


    public addToMioBlacklist(data: any): Observable<any> {
        this.setAuthorization();

        let url: string;

        url = this.url + 'mailInOne/addToMioBlacklist/';

        return this.mapRequest(
            this.http.post(url, data, { headers: this.headers })
        );
    }
}
