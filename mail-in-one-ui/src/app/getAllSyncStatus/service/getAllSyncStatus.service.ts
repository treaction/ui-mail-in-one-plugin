import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {
    TerraBaseService,
//    TerraDataTableRowInterface, TerraPagerInterface,
    TerraLoadingSpinnerService
} from '@plentymarkets/terra-components';
import {Observable} from 'rxjs';
// import {TerraDataTableBaseService} from '@plentymarkets/terra-components/app/components/tables/data-table/terra-data-table-base.service';
// import {PageingData} from '../pagerData/PageingData.interface';
// import {SyncStatusDataStructureInterface} from "../pagerData/SyncStatusDataStructure.interface";

@Injectable()

export class getAllSyncStatusService extends TerraBaseService {
// export class getAllSyncStatusService extends TerraDataTableBaseService<SyncStatusDataStructureInterface, PageingData> {

    constructor(private _loadingSpinnerService: TerraLoadingSpinnerService,
                private _http: Http
    ) {

        super(_loadingSpinnerService, _http, '/rest/');
        console.log('url='+this.url);
    }


    public getAllSyncStatus(): Observable<any> {

        this.setAuthorization();
        let url: string;

        url = this.url + 'mailInOne/getAllSyncStatus/';

        return this.mapRequest
        (
            this.http.get(url, {
                headers: this.headers,
                body: ''
            })
        );
    }


    public deleteAllSyncStatus(): Observable<any> {

        this.setAuthorization();
        let url: string;
        let data = '1';

        url = this.url + 'mailInOne/deleteAllSyncStatus/';

        return this.mapRequest
        (
            this.http.post(url, data, {headers: this.headers})
        );
    }
}
