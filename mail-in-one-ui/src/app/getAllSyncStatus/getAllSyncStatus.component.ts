import {
    Component,
    Input,
    OnInit
} from '@angular/core';
import {
    Translation,
    TranslationService
} from 'angular-l10n';

// PAGER
import { TerraPagerInterface } from '@plentymarkets/terra-components/app/components/pager/data/terra-pager.interface';
// PAGER

import { getAllSyncStatusService } from "./service/getAllSyncStatus.service";
import { TerraSimpleTableHeaderCellInterface, TerraSimpleTableRowInterface } from "@plentymarkets/terra-components";
import { PluginTerraBasicConfig } from '../mail-in-one.config';

@Component({
    selector: 'getAllSyncStatus',
    template: require('./getAllSyncStatus.component.html'),
    styles: [require('./getAllSyncStatus.component.scss')],
})

export class GetAllSyncStatusComponent implements OnInit {

    @Input() myTitle: string;
    private isLoading: boolean = true;
    private targetGroup: any;
    private headerList: Array<TerraSimpleTableHeaderCellInterface> = [];
    private rowList: Array<TerraSimpleTableRowInterface<any>> = [];
    // PAGER
    public pagingData:TerraPagerInterface;
    // PAGER

    constructor(public translation: TranslationService,
        private getAllSyncStatusService: getAllSyncStatusService,
        private _pluginTerraBasicConfig: PluginTerraBasicConfig) {

    }

    ngOnInit() {

        this.headerList = [
            {
                width: '20px',
                caption: 'Job-ID'
            },
            {
                width: '20px',
                caption: 'E-Mail'
            },
            {
                width: '20px',
                caption: 'Ordner-ID'
            },
            {
                width: '20px',
                caption: 'Ordner'
            },
            {
                width: '20px',
                caption: 'Newsletter-ID'
            },
            {
                width: '20px',
                caption: 'CRM-ID'
            },
            {
                width: '20px',
                caption: 'Status'
            },
            {
                width: '20px',
                caption: 'Startzeitpunkt'
            },
            {
                width: '20px',
                caption: 'Endzeitpunkt'
            }
        ]

        this.getAllSyncStatusService.getAllSyncStatus().subscribe(
            response => {
                let res = response;
                for (let sync_status of res) {
                    this.rowList.push(
                        {
                            cellList: [
                                {
                                    caption: sync_status["jobid"]
                                },
                                {
                                    caption: sync_status["email"]
                                },
                                {
                                    caption: sync_status["plenty_folder_id"]
                                },
                                {
                                    caption: sync_status["plenty_folder"]
                                },
                                {
                                    caption: sync_status["newsletter_id"]
                                },
                                {
                                    caption: sync_status["crm_id"]
                                },
                                {
                                    caption: sync_status["status"]
                                },
                                {
                                    caption: sync_status["starttime"]
                                },
                                {
                                    caption: sync_status["endtime"]
                                }
                            ]
                        }
                    )
                }
            },
            (error) => {
                this._pluginTerraBasicConfig.pluginTerraBasicComponent.callStatusEvent('Keine Daten vorhanden' + ': ' + error.statusText,
                    'danger');
                this.isLoading = false;
            }
        );
    }

    public refresh()
    {
        this.rowList = [];
        this.isLoading = true;
        this._pluginTerraBasicConfig.pluginTerraBasicComponent.callStatusEvent('Aktualisiere ...',
            'success');
        this.getAllSyncStatusService.getAllSyncStatus().subscribe(
            response => {
                let res = response;
                for (let sync_status of res) {
                    this.rowList.push(
                        {
                            cellList: [
                                {
                                    caption: sync_status["jobid"]
                                },
                                {
                                    caption: sync_status["email"]
                                },
                                {
                                    caption: sync_status["plenty_folder_id"]
                                },
                                {
                                    caption: sync_status["plenty_folder"]
                                },
                                {
                                    caption: sync_status["newsletter_id"]
                                },
                                {
                                    caption: sync_status["crm_id"]
                                },
                                {
                                    caption: sync_status["status"]
                                },
                                {
                                    caption: sync_status["starttime"]
                                },
                                {
                                    caption: sync_status["endtime"]
                                }
                            ]
                        }
                    )
                }
            },
            (error) => {
                this._pluginTerraBasicConfig.pluginTerraBasicComponent.callStatusEvent('Keine Daten vorhanden' + ': ' + error.statusText,
                    'danger');
                this.isLoading = false;
            }
        );

    }

    public deleteAllSyncStatus() {

        this._pluginTerraBasicConfig.pluginTerraBasicComponent.callStatusEvent('Kontaktverlauf wird gelöscht ...', 'success');

        this.getAllSyncStatusService.deleteAllSyncStatus().subscribe(
            response => {
                this._pluginTerraBasicConfig.pluginTerraBasicComponent.callStatusEvent('Kontaktverlauf gelöscht',
                    'success');
                this.isLoading = true;
            },
            (error) => {
                this._pluginTerraBasicConfig.pluginTerraBasicComponent.callStatusEvent('Ein Fehler ist aufgetreten' + ': ' + error.statusText,
                    'danger');
                this.isLoading = false;
            }
        )
    }
}
