export interface TerraPagerInterface {
    pagingUnit: any,
    totalsCount: any,
    page: any,
    itemsPerPage: any,
    lastPageNumber: any,
    firstOnPage: any,
    lastOnPage: any,
    isLastPage: boolean
}