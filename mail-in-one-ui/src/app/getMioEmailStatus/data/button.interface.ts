export interface ButtonInterface {
    icon?: string;
    caption?: string;
    tooltipText?: string;
    clickFunction: (event?: Event) => void;
    tooltipPlacement?: string;
    isPrimary?: boolean;
    isSecondary?: boolean;
    isTertiary?: boolean;
    isActive?: boolean;
    isDisabled?: boolean;
    isHidden?: boolean;
    isDivider?: boolean;
    isHighlighted?: boolean;
    isSmall?: boolean;
    isLarge?: boolean;
}
