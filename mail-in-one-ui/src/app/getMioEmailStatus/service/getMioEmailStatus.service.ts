import { Injectable } from '@angular/core';
import { Http, RequestOptions, Request, RequestMethod, Headers, Response, ResponseContentType } from '@angular/http';
import {
    TerraBaseService,
    TerraBaseData,
    TerraDataTableCellInterface,  // added terra-pager (not used)
    TerraDataTableRowInterface,   // added terra-pager (not used)
    TerraLoadingSpinnerService,
    TerraPagerInterface,          // added terra-pager (not used)
    TerraPagerParameterInterface  // added terra-pager (not used)
} from '@plentymarkets/terra-components';
import { Observable } from 'rxjs';
import { EmailListInterface } from "../data/emailist.interface";
import {EmailInterface} from '../data/email.interface';
import { catchError } from 'rxjs/operators';

@Injectable()

export class GetMioEmailStatusService extends TerraBaseService {

    constructor(private _loadingSpinnerService: TerraLoadingSpinnerService,
                private _http: Http) {
        super(_loadingSpinnerService, _http, '/rest/');
        // console.log(this.url);
    }


    public getMioEmailStatus(): Observable<any> {

        this.setAuthorization();
        let url: string;

        url = this.url + 'mailInOne/getMioEmailStatus/';

        return this.mapRequest
        (
            this.http.get(url, {
                headers: this.headers,
                body: ''
            })
        );
    }


    public getMioSingleEmailStatus(email: any): Observable<any> {
        // TODO: remove
        console.log("service getMioSingleEmailStatus email: " + email);

        this.setAuthorization();
        let url: string;
        url = this.url + 'mailInOne/getMioSingleEmailStatus/';

        // TODO: remove
        console.log( "headers.json = " + this.headers.toJSON().toString() );

        return this.mapRequest(
            this.http.post(url, email, {headers: this.headers})
        )
    }


    public unsubscribeContactByEmail(data: any): Observable<any> {
        this.setAuthorization();

        let url: string;

        url = this.url + 'mailInOne/uiUnsubscribeContactByEmail/';

        return this.mapRequest(
            this.http.post(url, data, {headers: this.headers})
        );
    }


    public getMioPluginOptionStandardBlacklist(): Observable<any> {
        this.setAuthorization();

        let url: string;

        url = this.url + 'mailInOne/getMioPluginOptionStandardBlacklist/';

        return this.mapRequest(
            this.http.get(url, {
                headers: this.headers,
                body: ''
            })
        );
    }


    public addToMioBlacklist(data: any): Observable<any> {
        this.setAuthorization();

        let url: string;

        url = this.url + 'mailInOne/addToMioBlacklist/';

        return this.mapRequest(
            this.http.post(url, data, {headers: this.headers})
        );
    }

    //  terra-pager stuff (not used)
    public dataToRowMapping(rowData: EmailListInterface): TerraDataTableRowInterface<TerraBaseData> {
        const cellList: Array<any> = [
            {
                data: rowData.email
            },
            {
                data: rowData.contact
            },
            {
                data: rowData.permission
            },
            {
                data: rowData.blocked
            },
            {
                data: rowData.blockingreason
            }
        ];
        return {
            cellList: cellList,
            data: rowData,
            clickFunction: (): void => alert('Row with id ${rowData.email} clicked')
        };
    }

}
