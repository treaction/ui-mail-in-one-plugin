import {
    Component,
    Input,
    OnInit
} from '@angular/core';
import {
    Translation,
    TranslationService
} from 'angular-l10n';

import { GetMioEmailStatusService } from './service/getMioEmailStatus.service';
import { EmailListInterface } from './data/emailist.interface';
import { BlacklistInterface } from './data/blacklist.interface';
import { UnsubscriberInterface } from './data/unsubscriber.interface';
import { TerraSimpleTableHeaderCellInterface, TerraSimpleTableRowInterface } from '@plentymarkets/terra-components';
import { PluginTerraBasicConfig } from '../mail-in-one.config';
import { TerraPagerInterface } from './data/terra-pager.interface';
import { ButtonInterface } from "./data/button.interface";
import { EmailInterface } from './data/email.interface';

@Component({
    selector: 'getMioEmailStatus',
    template: require('./getMioEmailStatus.component.html'),
    styles: [require('./getMioEmailStatus.component.scss')],
})

export class GetMioEmailStatusComponent implements OnInit {

    @Input() myTitle: string;
    private service: GetMioEmailStatusService;
    private hasBlacklist: boolean = false;
    private standardBlacklist: string = null;
    private ui_new_bl_email: string; // blacklist email
    private ui_new_us_email: string; // unsubscriber email
    private isLoading: boolean = false;
    private targetGroup: any;
    private headerList: Array<TerraSimpleTableHeaderCellInterface> = [];
    private rowList: Array<TerraSimpleTableRowInterface<any>> = [];
    private tempRowList: Array<TerraSimpleTableRowInterface<any>> = [];
    private emailList: Array<EmailListInterface> = [];
    public pagingData: TerraPagerInterface;

    constructor(
        public translation: TranslationService,
        private mioEmailStatusService: GetMioEmailStatusService,
        private _pluginTerraBasicConfig: PluginTerraBasicConfig
    ) {
        this.service = mioEmailStatusService;
    }

    ngOnInit() {
        this.getMioPluginOptionStandardBlacklist();
        console.debug('ngInit standardBlacklist=' + this.standardBlacklist);//TODO: remove
        console.debug('ngInit hasBlacklist=' + this.hasBlacklist.toString());//TODO: remove
        console.debug(this);

        this.headerList = [
            {
                width: '20px',
                caption: 'E-Mail-Adresse'
            },
            {
                width: '20px',
                caption: 'Newsletter bestellt am'
            },
            {
                width: '20px',
                caption: 'Art der Newsletter-Bestätigung'
            },
            // {
            //     width: '20px',
            //     caption: 'Abgemeldet am'
            // },
            {
                width: '20px',
                caption: 'E-Mail-Adresse blockiert am'
            },
            {
                width: '20px',
                caption: 'Grund der Blockierung'
            },
            {
                width: '20px',
                caption: 'Kontakt auf Blacklist'
            },
            {
                width: '20px',
                caption: 'Kontakt abmelden'
            }
        ];

        this.mioEmailStatusService.getMioEmailStatus().subscribe(
            response => {
                for (let subscrbr_status of response) {

                    // TODO : remove
                    console.log( 'blocked = |'+subscrbr_status["blocked"].trim() +' blocked is empty = '+ (subscrbr_status["blocked"]=='').valueOf().toString()  +  '|  !hasBlackList = ' + (!this.hasBlacklist).valueOf().toString()  );

                    let buttonBlacklist: Array<ButtonInterface> = [];
                    buttonBlacklist.push({
                        icon: (this.hasBlacklist) ? 'icon-order_note_customer' : 'icon-warning',
                        //tooltipText: (this.hasBlacklist) ? 'Kontakt zur Blacklist (ID ' + this.standardBlacklist + ') hinzufügen' : 'Bitte Standard-Blacklist unter Kontoeinstellungen angeben',
                        tooltipText: (this.hasBlacklist)
                            ? (this.hasBlacklist && (subscrbr_status.blocked.trim() == '' || subscrbr_status.blocked == undefined))
                                ? 'Kontakt zur Blacklist (ID ' + this.standardBlacklist + ') hinzufügen'
                                : 'Kontakt ist geblocked'
                            : 'Bitte Standard-Blacklist unter Kontoeinstellungen angeben',
                        tooltipPlacement: 'left',
                        isDisabled: (this.hasBlacklist && (subscrbr_status["blocked"].trim() == '' || subscrbr_status["blocked"] == undefined)) ? false : true,
                        isSecondary: true,
                        clickFunction: (): void => {
                            (this.hasBlacklist) ? this.addEmailToBlacklistInMio(subscrbr_status["email"]) : this.doNothing();

                        }
                    });

                    let buttonUnsubscribe: Array<ButtonInterface> = [];
                    buttonUnsubscribe.push({
                        icon: 'icon-listing_remove',
                        tooltipText: 'Kontakt vom Newsletterempfang abmelden',
                        tooltipPlacement: 'left',
                        isSecondary: true,
                        clickFunction: (): void => {
                            this.unsubscribeContactByEmail(subscrbr_status["email"]);
                        }
                    });

                    // console.log('subscrbr_status = ' + JSON.stringify(subscrbr_status));
                    this.rowList.push(
                        {
                            cellList: [
                                {
                                    caption: subscrbr_status["email"]
                                },
                                {
                                    caption: subscrbr_status["contact"]
                                },
                                {
                                    caption: subscrbr_status["permission"]
                                },
                                // {
                                //     caption: subscrbr_status["unsubscribed"]
                                // },
                                {
                                    caption: subscrbr_status["blocked"]
                                },
                                {
                                    caption: subscrbr_status["blockingreason"]
                                },
                                {
                                    buttonList: buttonBlacklist
                                },
                                {
                                    buttonList: buttonUnsubscribe
                                }
                            ]
                        }
                    );

                    /* terra pager does not do it's job this way :(
                    this.emailList.push(
                        {
                            email: subscrbr_status["email"],
                            contact: subscrbr_status["contact"],
                            permission: subscrbr_status["permission"],
                            unsubscribed: subscrbr_status["unsubscribed"],
                            blocked: subscrbr_status["blocked"],
                            blockingreason: subscrbr_status["blockingreason"]
                        }
                    );
                    */
                }
            },
            (error) => {
                this._pluginTerraBasicConfig.pluginTerraBasicComponent.callStatusEvent('Keine Daten vorhanden' + ': ' + error.statusText,
                    'danger');
                this.isLoading = false;
            }
        );

        /* terra-pager :(
        this.pagingData = {
            pagingUnit: this.emailList,
            totalsCount: this.rowList.length,
            page: 1,
            itemsPerPage: 50,
            lastPageNumber: Math.round(this.rowList.length / 50),
            firstOnPage: 1,
            lastOnPage: 50,
            isLastPage: false
        };
         */

    }


    private isEmail(email: string) {
        let validemail: any = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
        return validemail.test(String(email).toLowerCase());
    }


    private isEmpty(value: any) {
        switch (typeof (value)) {
            case 'number':
                return (value.toString().length === 0);
            case 'string':
                return (value.length === 0);
            case 'boolean':
                return false;
            case 'undefined':
                return true;
            case 'object':
                return !value ? true : false;
            default:
                return !value ? true : false;
        }
    }


    public getMioSingleEmailStatus(email: string) {
        this.isLoading = true;
        this.tempRowList = this.rowList;

        let itemApi: EmailInterface = {
            email: email
        };

        let dataApi = {
            settings: itemApi
        };

        this.mioEmailStatusService.getMioSingleEmailStatus(dataApi).subscribe(
            response => {

                var addToList: boolean = false;

                try {
                    addToList = (JSON.stringify(response).length > 10) ? true :false;
                } catch (e) {
                    console.log('ERROR '+e.toString());
                }

                if (addToList) {
                    this.rowList.reverse();

                    for (let subscrbr_status of response) {

                        // TODO: remove
                        try {
                            console.log("record received : " + JSON.stringify(subscrbr_status));
                        } catch (e) {
                            console.log('ERROR: ' + e.toString());
                        }

                        let buttonBlacklist: Array<ButtonInterface> = [];
                        buttonBlacklist.push({
                            icon: (this.hasBlacklist) ? 'icon-order_note_customer' : 'icon-warning',
                            tooltipText: (this.hasBlacklist)
                                ? (this.hasBlacklist && (subscrbr_status.blocked.trim() == '' || subscrbr_status.blocked == undefined))
                                    ? 'Kontakt zur Blacklist (ID ' + this.standardBlacklist + ') hinzufügen'
                                    : 'Kontakt ist geblocked'
                                : 'Bitte Standard-Blacklist unter Kontoeinstellungen angeben',
                            tooltipPlacement: 'left',
                            isDisabled: (this.hasBlacklist && (subscrbr_status.blocked.trim() == '' || subscrbr_status.blocked == undefined)) ? false : true,
                            isSecondary: true,
                            clickFunction: (): void => {
                                (this.hasBlacklist) ? this.addEmailToBlacklistInMio(subscrbr_status.email) : this.doNothing();
                            }
                        });

                        let buttonUnsubscribe: Array<ButtonInterface> = [];
                        buttonUnsubscribe.push({
                            icon: 'icon-listing_remove',
                            tooltipText: 'Kontakt vom Newsletterempfang abmelden',
                            tooltipPlacement: 'left',
                            isSecondary: true,
                            clickFunction: (): void => {
                                this.unsubscribeContactByEmail(subscrbr_status.email);
                            }
                        });


                        // console.log('subscrbr_status = ' + JSON.stringify(subscrbr_status));
                        this.rowList.push(
                            {
                                cellList: [
                                    {
                                        caption: subscrbr_status.email
                                    },
                                    {
                                        caption: subscrbr_status.contact
                                    },
                                    {
                                        caption: subscrbr_status.permission
                                    },
                                    // {
                                    //     caption: subscrbr_status["unsubscribed"]
                                    // },
                                    {
                                        caption: subscrbr_status.blocked
                                    },
                                    {
                                        caption: subscrbr_status.blockingreason
                                    },
                                    {
                                        buttonList: buttonBlacklist
                                    },
                                    {
                                        buttonList: buttonUnsubscribe
                                    }
                                ]
                            }
                        );
                    /*
                    terra pager does not do it's job this way :(
                                this.emailList.push(
                                    {
                                        email: subscrbr_status["email"],
                                        contact: subscrbr_status["contact"],
                                        permission: subscrbr_status["permission"],
                                        unsubscribed: subscrbr_status["unsubscribed"],
                                        blocked: subscrbr_status["blocked"],
                                        blockingreason: subscrbr_status["blockingreason"]
                                    }
                                );
                    */
                    }
                    this.rowList.reverse();
                }
            },
            (error) => {
                this._pluginTerraBasicConfig.pluginTerraBasicComponent.callStatusEvent('E-Mail Adresse ' + email + ' nicht gefunden' + ': ' + error.statusText,
                    'danger');
                this.rowList = this.tempRowList;
                this.isLoading = false;
            }
        );
    }


    public getMioPluginOptionStandardBlacklist() {

        this.mioEmailStatusService.getMioPluginOptionStandardBlacklist().subscribe(
            response => {
                this.standardBlacklist = response;
                if (this.standardBlacklist.toString().length > 0) {
                    this.hasBlacklist = true;
                }
                console.debug("1. standard blacklist is " + this.standardBlacklist); // TODO: remove
                console.debug("1. hasBlacklist is " + (this.hasBlacklist) ? 'wahr' : 'falsch'); // TODO: remove
            }
        );
        console.debug("2. standard blacklist is " + this.standardBlacklist); // TODO: remove
        console.debug("2. hasBlacklist is " + (this.hasBlacklist) ? 'wahr' : 'falsch'); // TODO: remove
    }


    public addEmailToBlacklistInMio(email: string) {
        this.isLoading = true;
        this.getMioPluginOptionStandardBlacklist();
        // this.setBlacklistBooleans(); // TODO: remove

        // TODO: remove
        console.log("ADD TO BLACKLIST #" + this.standardBlacklist + "  EMAIL=" + email);

        // if (this.isEmpty(this.ui_add_to_blacklist_id || this.isEmpty(this.ui_new_bl_email))) {
        if (this.standardBlacklist === null) {
            this._pluginTerraBasicConfig.pluginTerraBasicComponent.callStatusEvent('Bitte Mail-In-One Standard-Blacklist in Kontoeinstellungen setzen',
                'danger');
            return false;
        }

        if (this.isEmpty(email)) {
            this._pluginTerraBasicConfig.pluginTerraBasicComponent.callStatusEvent('Bitte geben Sie eine Email-Adresse an',
                'danger');
            return false;
        }

        let blacklistid = this.standardBlacklist;

        let itemApi: BlacklistInterface = {
            id: blacklistid.toString(),
            entries: email
        };

        // TODO: remove
        console.log('blacklist-entry to save = ' + btoa(JSON.stringify(itemApi)));

        let dataApi = {
            settings: itemApi
        };

        // TODO: remove
        console.log('dataApi = ' + dataApi);

        this.service.addToMioBlacklist(dataApi).subscribe(
            response => {
                this._pluginTerraBasicConfig.pluginTerraBasicComponent.callStatusEvent('Blacklist-Eintrag hinzugefügt.',
                    'success');
                this.isLoading = false;

                var today = new Date();
                var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
                var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
                var dateTime : any = date+' '+time;

                this.rowList.forEach((i,j) => {
                    if(i.cellList[0].caption === email ) {
                        console.log('email '+ email +' found');
                        i.cellList[3].caption = dateTime;
                        i.cellList[4].caption = <any>'blacklist';
                        i.cellList[5].buttonList[0].isDisabled = true;
                        i.cellList[5].buttonList[0].tooltipText = 'Kontakt wurde der Blacklist hinzugefügt';
                    }
                })
            },
            (error) => {
                this._pluginTerraBasicConfig.pluginTerraBasicComponent.callStatusEvent('Der Eintrag konnte nicht  gespeichert werden.' + ': ' + error.statusText,
                    'danger');
                this.isLoading = false;
            }
        );
    }


    public unsubscribeContactByEmail(email: string) {

        this.isLoading = true;

        if (this.isEmpty(email)) {
            this._pluginTerraBasicConfig.pluginTerraBasicComponent.callStatusEvent('Bitte geben Sie eine Email-Adresse an',
                'danger');
            return false;
        }

        if (!this.isEmail(email)) {
            this._pluginTerraBasicConfig.pluginTerraBasicComponent.callStatusEvent('Bitte geben Sie eine gültige Email-Adresse an.',
                'danger');
            return false;
        }


        let itemApi: UnsubscriberInterface = {
            email: email
        };

        // TODO: may be remove or leave it: decode in console with atob( string )  to check settings
        console.log('Unsubscriber entry to save (itemApi) = ' + btoa(JSON.stringify(itemApi)));

        let dataApi = {
            settings: itemApi
        };

        console.log('Unsubscriber entry to save (dataApi) = ' + btoa(JSON.stringify(dataApi)));

        this.service.unsubscribeContactByEmail(dataApi).subscribe(
            response => {
                this._pluginTerraBasicConfig.pluginTerraBasicComponent.callStatusEvent('Abmelder-Eintrag hinzugefügt.',
                    'success');

                this.rowList.forEach((i,j) => {
                    if(i.cellList[0].caption === email ) {
                        console.log('unsubscribe email '+ email +' found');
                        i.cellList[6].buttonList[0].isDisabled = true; // unsubscribe button disabled
                        i.cellList[6].buttonList[0].tooltipText = 'Kontakt wurde vom Newsletterempfang ausgeschlossen';
                        i.cellList[5].buttonList[0].isDisabled = true;
                        i.cellList[5].buttonList[0].tooltipText = ' Blacklisting nicht möglich: Kontakt wurde vom Newsletterempfang ausgeschlossen.';
                    }
                })

                this.isLoading = false;
            },
            (error) => {
                this._pluginTerraBasicConfig.pluginTerraBasicComponent.callStatusEvent('Der Abmelder-Eintrag konnte nicht gespeichert werden.' + ': ' + error.statusText,
                    'danger');
                this.isLoading = false;
            }
        );

    }

    public doNothing() {
        return;
    }


}
