import {
    Component,
    OnInit
} from '@angular/core';
import {
    LocaleService,
    Translation,
    TranslationService
} from 'angular-l10n';
import { PluginTerraBasicConfig } from './mail-in-one.config';

@Component({
               selector: 'mail-in-one-app',
               template: require('./mail-in-one.component.html'),
               styles:   [require('./mail-in-one.component.scss')],
           })
export class PluginTerraBasicComponent implements OnInit
{
    private action:any = this.getUrlVars()['action'];

    public constructor(public translation:TranslationService,
                       public locale:LocaleService,
                       private _pluginTerraBasicConfig:PluginTerraBasicConfig)
    {


    }

    ngOnInit()
    {
        this._pluginTerraBasicConfig.pluginTerraBasicComponent = this;
    }

    private getUrlVars()
    {
        var vars = {};

        window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(substring:string, ...args:any[]):string
        {
            vars[args[0]] = args[1];
            return;
        });

        return vars;
    }

    public callStatusEvent(message, type)
    {
        let detail = {
            type:    type,
            message: message
        };

        let customEvent:CustomEvent = new CustomEvent('status', {detail: detail});

        window.parent.window.parent.window.dispatchEvent(customEvent);
    }
}
