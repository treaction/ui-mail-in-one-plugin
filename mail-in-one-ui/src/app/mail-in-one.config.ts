import { Injectable } from '@angular/core';
import { PluginTerraBasicComponent } from './mail-in-one.component';

@Injectable()
export class PluginTerraBasicConfig
{
    public get pluginTerraBasicComponent():PluginTerraBasicComponent
    {
        return this._pluginTerraBasicComponent;
    }

    public set pluginTerraBasicComponent(value:PluginTerraBasicComponent)
    {
        this._pluginTerraBasicComponent = value;
    }

    private _pluginTerraBasicComponent:PluginTerraBasicComponent;

    constructor()
    {}
}