import {
    Component,
    Input,
    OnInit
} from '@angular/core';
import {
    Translation,
    TranslationService
} from 'angular-l10n';

import { TargetGroupService} from "./service/targetGroup.service";
import {TerraSimpleTableHeaderCellInterface, TerraSimpleTableRowInterface} from "@plentymarkets/terra-components";
import { PluginTerraBasicConfig } from '../mail-in-one.config';

@Component({
               selector: 'targetGroup',
               template: require('./targetGroup.component.html'),
               styles:   [require('./targetGroup.component.scss')],
           })
export class TargetGroupComponent implements OnInit
{

    @Input() myTitle:string;
    private isLoading:boolean = true;
    private targetGroup:any;
    private headerList:Array<TerraSimpleTableHeaderCellInterface> = [];
    private rowList:Array<TerraSimpleTableRowInterface<any>> = [];
    constructor( public translation:TranslationService,
                 private TargetGroupService:TargetGroupService,
                 private _pluginTerraBasicConfig:PluginTerraBasicConfig)
    {

    }

    ngOnInit()
    {

        this.headerList = [
                           {
                               width:  '20px',
                               caption: 'id'
                           },
                           {
                               width:   '20px',
                               caption: 'Name der Verteilerliste'
                           },
                           {
                               width:   '20px',
                               caption: 'Name der Kontaktfilter'
                           },
                           {
                               width:   '20px',
                               caption: 'Filter Id'
                           },
                           {
                               width:   '20px',
                               caption: 'Aktive Kontakte'
                           },
                           {
                               width:   '20px',
                               caption: 'Kontakte'
                           },
                           {
                               width:   '20px',
                               caption: 'Erstellung'
                           },
                           {
                               width:   '20px',
                               caption: 'aktualisieren'
                           }
                       ]

        this.TargetGroupService.getTargetGroupService().subscribe(
            response => {
                let res =  response['targetgroup'];
                for (let targetgroup of res) {
                    this.rowList.push(
                        {

                            cellList: [

                                {
                                    caption: targetgroup["id"]
                                },
                                {
                                    caption: targetgroup["name"]
                                },
                                {
                                    caption: targetgroup["contact_filter_name"]
                                },
                                {
                                    caption: targetgroup["contact_filter_id"]
                                },
                                {
                                    caption: targetgroup['count_active_contacts']
                                },
                                {
                                    caption: targetgroup['count_contacts']
                                },
                                {
                                    caption: targetgroup['created']
                                },
                                {
                                    caption: targetgroup['updated']
                                }
                            ]
                        }
                    )
                }
            },
            (error) => {
                this._pluginTerraBasicConfig.pluginTerraBasicComponent.callStatusEvent('Verteiler Listen sind nicht geladen.'+ ': ' + error.statusText,
                    'danger');
                this.isLoading = false;
            }
        );
    }

    public refresh() {
        this.rowList = [];
        this._pluginTerraBasicConfig.pluginTerraBasicComponent.callStatusEvent('Aktualisiere ...','success');

        this.TargetGroupService.getTargetGroupService().subscribe(
            response => {
                let res =  response['targetgroup'];
                for (let targetgroup of res) {
                    this.rowList.push(
                        {

                            cellList: [

                                {
                                    caption: targetgroup["id"]
                                },
                                {
                                    caption: targetgroup["name"]
                                },
                                {
                                    caption: targetgroup["contact_filter_name"]
                                },
                                {
                                    caption: targetgroup["contact_filter_id"]
                                },
                                {
                                    caption: targetgroup['count_active_contacts']
                                },
                                {
                                    caption: targetgroup['count_contacts']
                                },
                                {
                                    caption: targetgroup['created']
                                },
                                {
                                    caption: targetgroup['updated']
                                }
                            ]
                        }
                    )
                }
            },
            (error) => {
                this._pluginTerraBasicConfig.pluginTerraBasicComponent.callStatusEvent('Verteiler Listen sind nicht geladen.'+ ': ' + error.statusText,
                    'danger');
                this.isLoading = false;
            }
        );
    }
}
