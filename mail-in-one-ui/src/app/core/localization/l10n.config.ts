import {
    L10nConfig,
    ProviderType,
    StorageStrategy
} from 'angular-l10n';

export const l10nConfig:L10nConfig = getL10nConfig();

function getL10nConfig():L10nConfig
{
    let langInLocalStorage:string = localStorage.getItem('plentymarkets_lang_');
    let lang:string = null;

    if(langInLocalStorage !== null)
    {
        lang = langInLocalStorage;
    }
    else
    {
        lang = navigator.language.slice(0, 2).toLocaleLowerCase();

        if(lang !== 'de' && lang !== 'en')
        {
            lang = 'en';
        }

        localStorage.setItem('plentymarkets_lang_', lang);
    }

    let prefix:string = null;
    let terraComponentsLocalePrefix:string = null;
    let testPrefix:string = null;

    // Definitions for i18n
    if(process.env.ENV === 'production')
    {
        prefix = 'assets/lang/locale_';
        terraComponentsLocalePrefix = 'assets/lang/terra-components/locale_';
        testPrefix = 'assets/lang/terra-components/test_'
    }
    else
    {
        // prefix = 'src/app/assets/lang/locale_';
        prefix = 'src/assets/lang/locale_'; // https://forum.plentymarkets.com/t/termin-heute/573713/19 change 2020-01-15
        // terraComponentsLocalePrefix = 'node_modules/@plentymarkets/terra-components/app/assets/lang/locale_';
        terraComponentsLocalePrefix = 'node_modules/@plentymarkets/terra-components/assets/lang/locale_'; // https://forum.plentymarkets.com/t/termin-heute/573713/19 change 2020-01-15
        testPrefix = 'src/app/assets/lang/test_'
    }

    return {
        locale:      {
            languages: [
                {
                    code: 'en',
                    dir:  'ltr'
                },
                {
                    code: 'de',
                    dir:  'ltr'
                }
            ],
            language:  lang,
            storage:   StorageStrategy.Cookie
        },
        translation: {
            providers:            [
                {
                    type:   ProviderType.Static,
                    prefix: prefix
                },
                {
                    type:   ProviderType.Static,
                    prefix: terraComponentsLocalePrefix
                },
                {
                    type:   ProviderType.Static,
                    prefix: testPrefix
                }
            ],
            caching:              true,
            composedKeySeparator: '.',
            i18nPlural:           false
        }
    };
}
