export interface AccountInterface
{
    firstname:string;
    lastname:string;
    company:string;
    phone:string;
    street:string;
    zip:string;
    city:string;
    country:string;
    email:string;
}