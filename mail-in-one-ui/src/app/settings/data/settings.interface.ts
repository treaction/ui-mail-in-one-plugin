export interface SettingsInterface {
    id: number;
    apiKey: string;
    createdAt: string;
    loginurl: string;
    cronjob: number;
    pickedValueModus: string;
    quantity: number;
    logcontactprocess: string;
    ui_realtime_doi: string;
    ui_standard_blacklist_id: string; // actually a number
    ui_blocked_contacts_to_blacklist: string; // "true" or "false"
}