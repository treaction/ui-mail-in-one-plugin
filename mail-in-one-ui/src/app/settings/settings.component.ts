import {
    Component,
    OnInit
} from '@angular/core';
import {
    Translation,
    TranslationService
} from 'angular-l10n';

import { SettingsService } from './service/settings.service';
import { SettingsInterface } from './data/settings.interface';
import { AccountInterface } from './data/account.interface';
import { PluginTerraBasicConfig } from '../mail-in-one.config';
import { isNullOrUndefined, isNumber } from "util";
import { SelectBoxValueInterface } from "../event/data/select-box.interface";


@Component({
    selector: 'settings',
    template: require('./settings.component.html'),
    styles: [require('./settings.component.scss')],
})
export class SettingsComponent implements OnInit {
    private apiKey: string = '---';
    private loginurl: string = '---';
    private cronjob: number = 5;
    private firstname: string;
    private lastname: string;
    private company: string;
    private phone: string;
    private street: string;
    private zip: string;
    private city: string;
    private country: string;
    private email: string;
    private service: SettingsService;
    private isLoading: boolean = true;
    private isDisabled: boolean = false;
    private isNewUser: boolean = false;
    private isUrl: boolean = true;
    private hasBlacklist: boolean = false;
    private hasStandardBlacklist: boolean = false;

    private _selectableOptionTypesModus: Array<SelectBoxValueInterface> = [];
    private _mioContactProcessLogModeOptions: Array<SelectBoxValueInterface> = [];
    private _mio_ui_realtime_doi: Array<SelectBoxValueInterface> = [];
    private _pickedValueModus: string;
    private _isPickedValueModus: string;
    private quantity: number;
    private logcontactprocess: string;
    private ui_realtime_doi: string;
    private ui_standard_blacklist: string = null; // WEB-2446
    private _ui_blacklist_list: Array<SelectBoxValueInterface> = []; // WEB-2446
    private _ui_blocked_contacts_to_blacklist: Array<SelectBoxValueInterface> = [];// WEB-2446
    private ui_cntct_to_blacklist_set: string = '';// WEB-2446

    constructor(public translation: TranslationService,
                private settingsService: SettingsService,
                private _pluginTerraBasicConfig: PluginTerraBasicConfig) {
        this.service = settingsService;
    }

    ngOnInit() {
        this.isLoading = false;
        this.apiKey = this.getCurrentApiKey();
        this.loginurl = this.getCurrentLoginUrl();
        this.cronjob = this.getCurrentCronjob();
        this.change();
        this.getButton();
        this.getSynchronisation();
        this.getMioPluginOptionUIValue();
        this.getMioPluginOptionUIRealtimeDoi();
        this.getUIMioPluginOptionBlockedContactToBlacklist(); // WEB-2446
        console.log('apky='+btoa(this.apiKey)+', lngth='+this.apiKey.length);
        if( this.apiKey.length > 20) {
            console.log('init l75 => apikey seems to be set, getting blacklists');
            this.getMioPluginOptionStandardBlacklist(); // WEB-2446
            this.getMioBlacklists();//WEB-2446
        } else {
            console.log('init l79 => apikey not set, NOT getting blacklists');
            // try to get blacklists anyway
            this.getMioPluginOptionStandardBlacklist(); // WEB-2446
            this.getMioBlacklists();//WEB-2446
        }
    }

    public refresh() {
        this._pluginTerraBasicConfig.pluginTerraBasicComponent.callStatusEvent('Aktualisiere ...', 'success');
        this._mioContactProcessLogModeOptions = [];
        this._selectableOptionTypesModus = [];
        this._mio_ui_realtime_doi = [];
        this._ui_blacklist_list = [];
        this._ui_blocked_contacts_to_blacklist = [];

        this.apiKey = this.getCurrentApiKey();
        this.loginurl = this.getCurrentLoginUrl();
        this.cronjob = this.getCurrentCronjob();
        this.change();
        this.getButton();
        this.getSynchronisation();
        this.getMioPluginOptionUIValue();
        this.getMioPluginOptionUIRealtimeDoi();
        this.getUIMioPluginOptionBlockedContactToBlacklist(); // WEB-2446
        this.getMioPluginOptionStandardBlacklist(); // WEB-2446
        this.getMioBlacklists();//WEB-2446
    }

    public saveSettings() {
        console.log('before save: ui_blocked_contacts_to_blacklist='+this.ui_cntct_to_blacklist_set);
        this.getCurrentCronjob(); // what for ??
        this.getButton(); // what for ??
        this.getSynchronisation(); // what for ??
        this.getMioPluginOptionUIValue(); // what for ??
        this.getMioPluginOptionUIRealtimeDoi(); // again: what for??
        // this.getUIMioPluginOptionBlockedContactToBlacklist(); // WEB-2446
        if( this.apiKey.length > 20) {
            console.log('save l103 => apikey seems to be set, saving blacklists');
            this.getMioPluginOptionStandardBlacklist(); // WEB-2446
            this.getMioBlacklists();//WEB-2446 - to be tested!
        } else {
            console.log('save l110 => not apikey set, NOT saving blacklists');
        }

        if (((this.apiKey).length < 10) && !this.isDisabled) {

            this.sendmails();
        } else {
            console.log('while save: ui_blocked_contacts_to_blacklist='+this.ui_cntct_to_blacklist_set);
            let itemApi: SettingsInterface = {
                id: 0,
                apiKey: this.apiKey,
                createdAt: '',
                loginurl: this.loginurl,
                cronjob: this.cronjob,
                pickedValueModus: this._pickedValueModus,
                quantity: this.quantity,
                logcontactprocess: this.logcontactprocess,
                ui_realtime_doi: this.ui_realtime_doi,
                ui_blocked_contacts_to_blacklist: this.ui_cntct_to_blacklist_set,
                ui_standard_blacklist_id: (this.ui_standard_blacklist == undefined || this.ui_standard_blacklist.length == 0) ? '' : this.ui_standard_blacklist
            };

            // TODO: may be remove or leave it: decode in console with atob( string )  to check settings
            console.log('settings to save = ' + btoa(JSON.stringify(itemApi)));

            let dataApi = {
                settings: itemApi
            };

            // if (!isNumber(this.cronjob)) {
            //   this._pluginTerraBasicConfig.pluginTerraBasicComponent.callStatusEvent(this.translation.translate('numberError'), 'danger');
            //  this.isLoading = false;
            //  }
            // if (isNumber(this.cronjob)) {
            this.service.saveSettings(dataApi).subscribe(
                response => {
                    this._pluginTerraBasicConfig.pluginTerraBasicComponent.callStatusEvent('Einstellungen gespeichert.',
                        'success');
                    this.isLoading = true;
                },
                (error) => {
                    this._pluginTerraBasicConfig.pluginTerraBasicComponent.callStatusEvent('Einstellungen nicht gespeichert.' + ': ' + error.statusText,
                        'danger');
                    this.isLoading = false;
                }
            );
            // }
        }
    }

    public sendmails() {
        let item: AccountInterface = {
            firstname: this.firstname,
            lastname: this.lastname,
            company: this.company,
            phone: this.phone,
            street: this.street,
            zip: this.zip,
            city: this.city,
            country: this.country,
            email: this.email
        };

        let data = {
            account: item
        };
        let firstname = this.firstname;
        if (isNullOrUndefined(firstname)) {
            this._pluginTerraBasicConfig.pluginTerraBasicComponent.callStatusEvent(this.translation.translate('firstnameError'), 'danger');
            this.isLoading = false;
        }
        if (isNullOrUndefined(this.lastname)) {
            this._pluginTerraBasicConfig.pluginTerraBasicComponent.callStatusEvent(this.translation.translate('lastnameError'), 'danger');
            this.isLoading = false;
        }
        if (isNullOrUndefined(this.company)) {
            this._pluginTerraBasicConfig.pluginTerraBasicComponent.callStatusEvent(this.translation.translate('companyError'), 'danger');
            this.isLoading = false;
        }
        if (isNullOrUndefined(this.phone)) {
            this._pluginTerraBasicConfig.pluginTerraBasicComponent.callStatusEvent(this.translation.translate('phoneError'), 'danger');
            this.isLoading = false;
        }
        if (isNullOrUndefined(this.street)) {
            this._pluginTerraBasicConfig.pluginTerraBasicComponent.callStatusEvent(this.translation.translate('streetError'), 'danger');
            this.isLoading = false;
        }
        if (isNullOrUndefined(this.zip)) {
            this._pluginTerraBasicConfig.pluginTerraBasicComponent.callStatusEvent(this.translation.translate('zipError'), 'danger');
            this.isLoading = false;
        }
        if (isNullOrUndefined(this.city)) {
            this._pluginTerraBasicConfig.pluginTerraBasicComponent.callStatusEvent(this.translation.translate('cityError'), 'danger');
            this.isLoading = false;
        }
        if (isNullOrUndefined(this.country)) {
            this._pluginTerraBasicConfig.pluginTerraBasicComponent.callStatusEvent(this.translation.translate('countryError'), 'danger');
            this.isLoading = false;
        }
        if (isNullOrUndefined(this.email)) {
            this._pluginTerraBasicConfig.pluginTerraBasicComponent.callStatusEvent(this.translation.translate('emailError'), 'danger');
            this.isLoading = false;
        }
        if (!isNullOrUndefined(this.firstname) && !isNullOrUndefined(this.lastname) && !isNullOrUndefined(this.company) && !isNullOrUndefined(this.phone) && !isNullOrUndefined(this.street) && !isNullOrUndefined(this.zip) && !isNullOrUndefined(this.country) && !isNullOrUndefined(this.email)) {
            this.service.sendmails(data).subscribe(
                response => {
                    this._pluginTerraBasicConfig.pluginTerraBasicComponent.callStatusEvent(this.translation.translate('successSendMail'),
                        'success');
                    this.isDisabled = true;
                    this.isLoading = true;
                },
                (error) => {
                    this._pluginTerraBasicConfig.pluginTerraBasicComponent.callStatusEvent('Email ist raus und Sie erhalten in Kürze eine Bestätigung mit Ihrem Benutzer-Login, Kennwort und Ihrem API Key' + ': ' + error.statusText,
                        'danger');
                    this.isLoading = false;
                }
            );
        }

    }

    public getCurrentApiKey() {
        this.settingsService.getCurrentApiKey().subscribe(
            response => {
                this.apiKey = response;
            }
        );
        console.log("l245(apky)="+btoa(this.apiKey));
        return this.apiKey;
    }

    public getCurrentLoginUrl() {
        this.settingsService.getCurrentLoginUrl().subscribe(
            response => {
                this.loginurl = response;
            }
        );
        return this.loginurl;
    }

    public getCurrentCronjob() {
        this.settingsService.getCurrentCronjob().subscribe(
            response => {
                this.cronjob = response;
            }
        );
        return this.cronjob;
    }

    public change() {
        if ((this.apiKey).length > 10) {
            this.isDisabled = !this.isDisabled;
        }


    }

    public getButton() {
        this.loginurl = this.getCurrentLoginUrl();
        if ((this.loginurl).length > 10) {
            this.isUrl = true;
        }


    }

    public newUser() {
        this.isNewUser = !this.isNewUser;
    }

    public mailInOne() {

        window.open(this.loginurl, "_blank");
    }

    public getSynchronisation() {
        this.settingsService.getSynchronisation().subscribe(
            response => {
                this._selectableOptionTypesModus.push(
                    {
                        value: response['syncOption'][0],
                        caption: response['syncOption'][0]
                    },
                    {
                        value: 'Massen',
                        caption: 'Massen'
                    },
                    {
                        value: 'Einzel',
                        caption: 'Einzel'
                    }
                );
                this._isPickedValueModus = response['syncOption'][0];
                this.quantity = response['syncOption'][1];
            }
        );
        return this.quantity + '--' + this._isPickedValueModus;
    }

    public getMioPluginOptionUIValue() {
        // _mioContactProcessLogModeOptions

        this.settingsService.getMioPluginOptionUIValue().subscribe(
            response => {
                this._mioContactProcessLogModeOptions.push(
                    {
                        value: response['ui_log_contact_process'][0],
                        caption: response['ui_log_contact_process'][0]
                    },
                    {
                        value: response['ui_log_contact_process'][1],
                        caption: response['ui_log_contact_process'][1]
                    }
                );

                // value found in Model|Table MioOptionsKv as default
                this.logcontactprocess = response['ui_log_contact_process'][0];

            }
        );
        return this.logcontactprocess;
    }

    public getMioPluginOptionUIRealtimeDoi() {

        this.settingsService.getMioPluginOptionUIRealtimeDoi().subscribe(
            response => {
                this._mio_ui_realtime_doi.push(
                    {
                        value: response['ui_realtime_doi'][0],
                        caption: response['ui_realtime_doi'][0]
                    },
                    {
                        value: response['ui_realtime_doi'][1],
                        caption: response['ui_realtime_doi'][1]
                    }
                );

                // value found in Model|Table MioDoiSubscriberStatus as default
                this.ui_realtime_doi = response['ui_realtime_doi'][0];

            }
        );
        return this._mio_ui_realtime_doi;
    }

    // true / false
    public getUIMioPluginOptionBlockedContactToBlacklist() {
        this.ui_cntct_to_blacklist_set = ''; // added 191125
        if(this.apiKey.length > 20 ) // added 191125
        {
            this.settingsService.getUIMioPluginOptionBlockedContactToBlacklist().subscribe(
                response => {
                    this._ui_blocked_contacts_to_blacklist.push(
                        {
                            value: response['ui_cntct_to_blacklist'][0],
                            caption: response['ui_cntct_to_blacklist'][0]
                        },
                        {
                            value: response['ui_cntct_to_blacklist'][1],
                            caption: response['ui_cntct_to_blacklist'][1]
                        }
                    );

                    // value found in Model|Table MioDoiSubscriberStatus as default
                    this.ui_cntct_to_blacklist_set = response['ui_cntct_to_blacklist'][0];

                }
            );
        }
        console.log('getUIMioPluginOptionBlockedContactToBlacklist.ui_cntct_to_blacklist_set = ' + this.ui_cntct_to_blacklist_set);
        return this.ui_cntct_to_blacklist_set;
    }

    public getMioPluginOptionStandardBlacklist() {
        this.settingsService.getMioPluginOptionStandardBlacklist().subscribe(
            response => {
                this.ui_standard_blacklist = response;
            }
        );

        if (this.ui_standard_blacklist !== null && this.ui_standard_blacklist != '0') {
            this.hasStandardBlacklist = true;
        }

        return this.ui_standard_blacklist;
    }

    // set standard-blacklist as first vaule in select-box
    public getMioBlacklists() {
        if (this.apiKey.length > 20) {
            this.settingsService.getMioBlacklists().subscribe(
                // _blacklistSelectBox
                response => {
                    if (this.ui_standard_blacklist !== null) {

                        var blacklist_id = (this.ui_standard_blacklist == '0')
                            ? '0'
                            : this.ui_standard_blacklist;

                        var standard_bl_name = 'Standard-Blacklist';
                        var standard_count_entries = '';

                        for (let bl of response) {
                            if (this.ui_standard_blacklist == bl["id"]) {
                                standard_bl_name = bl["name"];
                                standard_count_entries = ' / ' + bl["count_entries"]; // incorrect number => removed from display!
                            }
                        }

                        // add settings blacklist as first entry
                        this._ui_blacklist_list.push(
                            {
                                value: blacklist_id,
                                caption: standard_bl_name + ' (ID ' + this.ui_standard_blacklist + ')'
                            }
                        );
                    }
                    ;


                    for (let bl of response) {
                        console.log('bl = ' + JSON.stringify(bl));

                        if (bl["id"] != this.ui_standard_blacklist) {
                            this._ui_blacklist_list.push(
                                {
                                    value: bl["id"],
                                    caption: bl["name"] + ' (ID ' + bl["id"] + ')'
                                }
                            );
                        }
                    }

                    if (this._ui_blacklist_list.length > 0) {
                        this.hasBlacklist = true;
                    }
                    // TODO: remove
                    console.log('_ui_blacklist_list = ' + JSON.stringify(this._ui_blacklist_list));
                },
                (error) => {
                    this.hasBlacklist = false;
                    let err_stat = (error.statusText != '' && error.statusText != undefined) ? error.statusText : '';
                    this._pluginTerraBasicConfig.pluginTerraBasicComponent.callStatusEvent('Keine Blacklists vorhanden' + ': ' + err_stat,
                        'danger');
                    this.isLoading = false;
                }
            );
            return this._ui_blacklist_list;
        } else {
            return this._ui_blacklist_list;
        }
    }



}
