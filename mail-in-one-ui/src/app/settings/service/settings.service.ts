import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import {
    TerraBaseService,
    TerraLoadingSpinnerService
} from '@plentymarkets/terra-components';
import { Observable } from 'rxjs';
import { SettingsInterface } from '../data/settings.interface'

@Injectable()
export class SettingsService extends TerraBaseService {
    private _currentSettings: any;

    constructor(private _loadingSpinnerService: TerraLoadingSpinnerService,
        private _http: Http) {
        super(_loadingSpinnerService, _http, '/rest/');
    }

    // get data from options table WEB-2446
    public getMioPluginOptionStandardBlacklist(): Observable<any> {
        this.setAuthorization();

        let url: string;

        url = this.url + 'mailInOne/getMioPluginOptionStandardBlacklist/';

        return this.mapRequest(
            this.http.get(url, {
                headers: this.headers,
                body: ''
            })
        );
    }

    public getMioBlacklists(): Observable<any> {

        let url: string;

        url = this.url + 'mailInOne/getMioBlacklists/';

        return this.mapRequest(
            this.http.get(url, {
                headers: this.headers,
                body: ''
            })
        );
    }


    // get data from options table WEB-2446
    public getUIMioPluginOptionBlockedContactToBlacklist(): Observable<any> {
        this.setAuthorization();

        let url: string;

        url = this.url + 'mailInOne/getUIMioPluginOptionBlockedContactToBlacklist/';

        return this.mapRequest(
            this.http.get(url, {
                headers: this.headers,
                body: ''
            })
        );
    }


    // get data from options table - this option is not used yet (3.1.0)
    public getUISyncPlentyBlockedContacts(): Observable<any> {
        this.setAuthorization();

        let url: string;

        url = this.url + 'mailInOne/getUISyncPlentyBlockedContacts/';

        return this.mapRequest(
            this.http.get(url, {
                headers: this.headers,
                body: ''
            })
        );
    }


    public getSynchronisation(): Observable<any> {
        this.setAuthorization();

        let url: string;

        url = this.url + 'mailInOne/getSynchronisation/';

        return this.mapRequest(
            this.http.get(url, {
                headers: this.headers,
                body: ''
            })
        );
    }


    public getMioPluginOptionUIValue(): Observable<any> {
        this.setAuthorization();

        let url: string;

        url = this.url + 'mailInOne/getMioPluginOptionUIValue/';

        return this.mapRequest(
            this.http.get(url, {
                headers: this.headers,
                body: ''
            })
        );
    }

    public getMioPluginOptionUIRealtimeDoi(): Observable<any> {
        this.setAuthorization();

        let url: string;

        url = this.url + 'mailInOne/getMioPluginOptionUIRealtimeDoi/';

        return this.mapRequest(
            this.http.get(url, {
                headers: this.headers,
                body: ''
            })
        );
    }


    public getCurrentApiKey(): Observable<any> {
        this.setAuthorization();

        let url: string;

        url = this.url + 'mailInOne/apikey/';

        return this.mapRequest(
            this.http.get(url, {
                headers: this.headers,
                body: ''
            })
        );
    }

    public getCurrentCronjob(): Observable<any> {
        this.setAuthorization();

        let url: string;

        url = this.url + 'mailInOne/cronjob/';

        return this.mapRequest(
            this.http.get(url, {
                headers: this.headers,
                body: ''
            })
        );
    }
    public getCurrentLoginUrl(): Observable<any> {
        this.setAuthorization();

        let url: string;

        url = this.url + 'mailInOne/loginurl/';

        return this.mapRequest(
            this.http.get(url, {
                headers: this.headers,
                body: ''
            })
        );
    }

    public saveSettings(data: any): Observable<any> {
        this.setAuthorization();

        let url: string;

        url = this.url + 'mailInOne/settings/';

        return this.mapRequest(
            this.http.post(url, data, { headers: this.headers })
        );
    }

    public sendmails(data: any): Observable<any> {
        this.setAuthorization();

        let url: string;

        url = this.url + 'mailInOne/sendmails/';

        return this.mapRequest(
            this.http.post(url, data, { headers: this.headers })
        );
    }

    public get currentSettings(): SettingsInterface {
        return this._currentSettings;
    }
}
