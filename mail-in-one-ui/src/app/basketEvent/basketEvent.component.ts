import {
    Component,
    OnInit
} from '@angular/core';
import {
    Translation,
    TranslationService,
    Language
} from 'angular-l10n';

import { TerraSimpleTableHeaderCellInterface, TerraSimpleTableRowInterface } from "@plentymarkets/terra-components";
import { PluginTerraBasicConfig } from '../mail-in-one.config';
import { SelectBoxValueInterface } from "./data/select-box.interface";
import { ButtonInterface } from "./data/button.interface";
import { BasketEventService } from "./service/basketEvent.service";
import { BasketEventInterface } from "../basketEvent/data/basketEvent.interface";

@Component({
    selector: 'basketevent',
    template: require('./basketEvent.component.html')
})

export class BasketEventComponent implements OnInit {
    @Language()
    protected lang: string;
    private isLoading: boolean = true;
    private new: boolean = false;
    private edit: boolean = false;
    private isActiv: boolean = false;
    private ActivString: string;
    private integration: string;
    private kontaktereignis: string;
    private kontaktereignisID: number;

    private eventName: string;
    private _selectableOptionTypesListMioEventName: Array<SelectBoxValueInterface> = [];
    private _pickedValueMioEventName: string;
    private _selectableOptionTypesListIntegration: Array<SelectBoxValueInterface> = [];
    private _pickedValueIntegration: string;
    private _selectableOptionTypesListEvent: Array<SelectBoxValueInterface> = [];
    private _pickedValueEvent: string;
    private _selectableOptionTypesListEventStatus: Array<SelectBoxValueInterface> = [];
    private _pickedValueEventStatus: any;
    private marketingAutomation: string;
    private _selectableOptionTypesListStatus: Array<SelectBoxValueInterface> = [];
    private _pickedValueStatus: boolean;

    private _idEvent: number;
    private _idEventName: string;
    private _editEventName: string;
    private _editSelectableOptionTypesListMioEventName: Array<SelectBoxValueInterface> = [];
    private _editPickedValueMioEventName: string;
    private _editSelectableOptionTypesListEventStatus: Array<SelectBoxValueInterface> = [];
    private _editPickedValueEventStatus: any;
    private _editSelectableOptionTypesListIntegration: Array<SelectBoxValueInterface> = [];
    private _editPickedValueIntegration: string;
    private _editSelectableOptionTypesListEvent: Array<SelectBoxValueInterface> = [];
    private _editPickedValueEvent: string;
    private editMarketingAutomation: string;
    private _editSelectableOptionTypesListIsAktiv: Array<SelectBoxValueInterface> = [];
    private _editPickedValueIsAktiv: boolean;

    private headerList: Array<TerraSimpleTableHeaderCellInterface> = [];
    private rowList: Array<TerraSimpleTableRowInterface<any>> = [];

    constructor(public translation: TranslationService,
                private BasketEventService: BasketEventService,
                private _pluginTerraBasicConfig: PluginTerraBasicConfig) {

    }

    ngOnInit() {


        this.headerList = [
            {
                width: '10px',
                caption: 'ID'
            },
            {
                width: '20px',
                caption: 'Name'
            },
            {
                width: '20px',
                caption: 'Plenty Ereignis'
            },
            {
                width: '20px',
                caption: 'Mail-In-One Ereignis'
            },
            {
                width: '20px',
                caption: 'Integration'
            },
            {
                width: '20px',
                caption: 'Kontaktereignis'
            },
            {
                width: '20px',
                caption: 'Marketing Automation ID'
            },
            {
                width: '20px',
                caption: 'Status'
            },
            {
                width: '20px',
                caption: 'Löschen'
            },
            {
                width: '20px',
                caption: 'Edit'
            }
        ];

        this.BasketEventService.getAllOrderEvent().subscribe(
            response => {
                for (let resDB of response['DB']) {
                    if (resDB["isActiv"] == 1) {
                        this.isActiv = true;
                        this.ActivString = 'Aktiv';
                    } else {
                        this.isActiv = false;
                        this.ActivString = 'Inaktiv';
                    }
                    let buttonList: Array<ButtonInterface> = [];
                    buttonList.push({
                        icon: "'icon-delete'",
                        caption: 'löschen',
                        clickFunction: (): void => {
                            this.delete(resDB["id"]);
                        }
                    });
                    let buttonEdit: Array<ButtonInterface> = [];
                    buttonEdit.push({
                        icon: "'icon-edit'",
                        caption: 'Edit',
                        clickFunction: (): void => {
                            this.update(resDB, response);
                        }
                    });
                    this.rowList.push(
                        {
                            cellList: [

                                {
                                    caption: resDB["id"]
                                },
                                {
                                    caption: resDB["name"]
                                },
                                {
                                    caption: resDB["event"]
                                },
                                {
                                    caption: resDB["mioEvent"]
                                },
                                {
                                    caption: resDB["integration"]
                                },
                                {
                                    caption: resDB["kontaktereignis"]
                                },
                                {
                                    caption: resDB["marketingAutomation"]
                                },
                                {
                                    caption: this.ActivString
                                },
                                {
                                    buttonList: buttonList
                                },
                                {
                                    buttonList: buttonEdit
                                }

                            ]
                        }
                    );
                }

                for (let resConatctEvent of response['conatctEvent']) {
                    this._selectableOptionTypesListEvent.push(
                        {
                            value: '',
                            caption: ''
                        },
                        {
                            value: resConatctEvent["id"][0],
                            caption: resConatctEvent["name"]
                        }
                    );
                }

            },
            (error) => {
                this._pluginTerraBasicConfig.pluginTerraBasicComponent.callStatusEvent('Ein Fehler ist aufgetreten' + ': ' + error.statusText,
                    'danger');
                this.isLoading = false;
            }
        );


        this._selectableOptionTypesListMioEventName.push(
            {
                value: '',
                caption: ''
            },
            {
                value: 'MIO-Neuer Auftrag',
                caption: 'MIO-Neuer Auftrag'
            },
        );


        this._selectableOptionTypesListEventStatus.push(
            {
                value: '',
                caption: ''
            },
            {
                value: 'Neuer Auftrag',
                caption: 'Neuer Auftrag'
            },
        );


        this.BasketEventService.getAllStatusOrder().subscribe(
            response => {
                for (let res of response) {
                    this._selectableOptionTypesListEventStatus.push(
                        {
                            value: res['names']['de'],
                            caption: res['names']['de']
                        }
                    );
                    this._selectableOptionTypesListMioEventName.push(
                        {
                            value: 'MIO-' + res['names']['de'],
                            caption: 'MIO-' + res['names']['de']
                        }
                    );

                }

            },
            (error) => {
                this._pluginTerraBasicConfig.pluginTerraBasicComponent.callStatusEvent('Ein Fehler ist aufgetreten' + ': ' + error.statusText,
                    'danger');
                this.isLoading = false;
            }
        );


        this._selectableOptionTypesListIntegration.push(
            {
                value: '',
                caption: ''
            },
            {
                value: 'Kontaktereignis',
                caption: 'Kontaktereignis'
            },
            {
                value: 'Marketing Program',
                caption: 'Marketing Program'
            }
        );


        this._selectableOptionTypesListStatus.push(
            {
                value: '1',
                caption: 'Aktiv'
            },
            {
                value: '0',
                caption: 'Inaktiv'
            }
        );
    }


    public refresh() {
        this.rowList = [];
        this._pluginTerraBasicConfig.pluginTerraBasicComponent.callStatusEvent('Aktualisiere ...', 'success');

        this.BasketEventService.getAllOrderEvent().subscribe(
            response => {
                for (let resDB of response['DB']) {
                    if (resDB["isActiv"] == 1) {
                        this.isActiv = true;
                        this.ActivString = 'Aktiv';
                    } else {
                        this.isActiv = false;
                        this.ActivString = 'Inaktiv';
                    }
                    let buttonList: Array<ButtonInterface> = [];
                    buttonList.push({
                        icon: "'icon-delete'",
                        caption: 'löschen',
                        clickFunction: (): void => {
                            this.delete(resDB["id"]);
                        }
                    });
                    let buttonEdit: Array<ButtonInterface> = [];
                    buttonEdit.push({
                        icon: "'icon-edit'",
                        caption: 'Edit',
                        clickFunction: (): void => {
                            this.update(resDB, response);
                        }
                    });

                    this.rowList.push(
                        {
                            cellList: [

                                {
                                    caption: resDB["id"]
                                },
                                {
                                    caption: resDB["name"]
                                },
                                {
                                    caption: resDB["event"]
                                },
                                {
                                    caption: resDB["mioEvent"]
                                },
                                {
                                    caption: resDB["integration"]
                                },
                                {
                                    caption: resDB["kontaktereignis"]
                                },
                                {
                                    caption: resDB["marketingAutomation"]
                                },
                                {
                                    caption: this.ActivString
                                },
                                {
                                    buttonList: buttonList
                                },
                                {
                                    buttonList: buttonEdit
                                }

                            ]
                        }
                    );
                }

                this._selectableOptionTypesListEvent = [];

                for (let resConatctEvent of response['conatctEvent']) {
                    this._selectableOptionTypesListEvent.push(
                        {
                            value: '',
                            caption: ''
                        },
                        {
                            value: resConatctEvent["id"][0],
                            caption: resConatctEvent["name"]
                        }
                    );
                }

            }
        );

        this.BasketEventService.getAllStatusOrder().subscribe(
            response => {
                for (let res of response) {
                    this._selectableOptionTypesListEventStatus.push(
                        {
                            value: res['names']['de'],
                            caption: res['names']['de']
                        }
                    );
                    this._selectableOptionTypesListMioEventName.push(
                        {
                            value: 'MIO-' + res['names']['de'],
                            caption: 'MIO-' + res['names']['de']
                        }
                    );

                }
            },
            (error) => {
                this._pluginTerraBasicConfig.pluginTerraBasicComponent.callStatusEvent('Ein Fehler ist aufgetreten' + ': ' + error.statusText,
                    'danger');
                this.isLoading = false;
            }
        );
    }


    public add() {
        this.new = true;
        this.edit = false;
    }


    public return() {
        this.new = false;
        this.edit = false;
    }


    public delete(_id) {

        let dataId = {
            orderEventId: _id
        };

        this.BasketEventService.deleteOrderEvent(dataId).subscribe(
            response => {
                this._pluginTerraBasicConfig.pluginTerraBasicComponent.callStatusEvent('Ereignis gelöscht.',
                    'success');
                this.isLoading = true;
            },
            (error) => {
                this._pluginTerraBasicConfig.pluginTerraBasicComponent.callStatusEvent('Ein Fehler ist aufgetreten' + ': ' + error.statusText,
                    'danger');
                this.isLoading = false;
            }
        );
    }


    public save() {
        let itemOrderEvent: BasketEventInterface = {
            id: 0,
            statusId: this._pickedValueEventStatus,
            event: this.eventName,
            mioEvent: this._pickedValueMioEventName,
            integration: this._pickedValueIntegration,
            kontaktereignis: this._pickedValueEvent,
            kontaktereignisID: 0,
            marketingAutomation: this.marketingAutomation,
            isActiv: this._pickedValueStatus
        };

        let dataEvent = {
            orderevent: itemOrderEvent
        };

        this.BasketEventService.saveOrderEvent(dataEvent).subscribe(
            response => {
                this._pluginTerraBasicConfig.pluginTerraBasicComponent.callStatusEvent('Einstellungen gespeichert.',
                    'success');
                this.isLoading = true;
            },
            (error) => {
                this._pluginTerraBasicConfig.pluginTerraBasicComponent.callStatusEvent('Einstellungen nicht gespeichert.' + ': ' + error.statusText,
                    'danger');
                this.isLoading = false;
            }
        );
    }

    public update(_event, _response) {

        this.edit = true;
        this.new = false;


        this._idEvent = _event["id"];
        this._idEventName = _event["event"];
        this._editEventName = _event["name"];
        this._editPickedValueIsAktiv = _event["isActiv"];
        this._editPickedValueMioEventName = _event["mioEvent"];
        this.editMarketingAutomation = _event["marketingAutomation"];
        this._editSelectableOptionTypesListEventStatus = [];
        this._editSelectableOptionTypesListIntegration = [];
        this._editSelectableOptionTypesListEvent = [];
        this._editSelectableOptionTypesListIsAktiv = [];
        this._editSelectableOptionTypesListMioEventName = [];


        this._editSelectableOptionTypesListEventStatus.push(
            {
                value: _event["event"],
                caption: _event["event"]
            }
        );
        this._editSelectableOptionTypesListEventStatus.push(
            {
                value: 'Neuer Auftrag',
                caption: 'Neuer Auftrag'
            }
        );

        this._editSelectableOptionTypesListMioEventName.push(
            {
                value: _event["event"],
                caption: _event["event"]
            }
        );
        this._editSelectableOptionTypesListMioEventName.push(
            {
                value: 'MIO-Neuer Auftrag',
                caption: 'MIO-Neuer Auftrag'
            }
        );
        for (let resStatusOrder of _response['statusOrder']) {
            this._editSelectableOptionTypesListEventStatus.push(
                {
                    value: 'MIO-' + resStatusOrder['names']['de'],
                    caption: 'MIO-' + resStatusOrder['names']['de']
                }
            );
            this._editSelectableOptionTypesListMioEventName.push(
                {
                    value: 'MIO-' + resStatusOrder['names']['de'],
                    caption: 'MIO-' + resStatusOrder['names']['de']
                }
            );

        }


        this._editSelectableOptionTypesListIntegration.push(
            {
                value: _event["integration"],
                caption: _event["integration"]
            }
        );
        this._editSelectableOptionTypesListIntegration.push(
            {
                value: 'Kontaktereignis',
                caption: 'Kontaktereignis'
            },
            {
                value: 'Marketing Programm',
                caption: 'Marketing Programm'
            }
        );


        this._editSelectableOptionTypesListEvent.push(
            {
                value: _event["kontaktereignis"],
                caption: _event["kontaktereignis"]
            }
        );
        for (let resConatctEvent of _response['conatctEvent']) {

            this._editSelectableOptionTypesListEvent.push(
                {
                    value: resConatctEvent["id"][0],
                    caption: resConatctEvent["name"]
                }
            );
        }


        this._editSelectableOptionTypesListIsAktiv.push(
            {
                value: _event["isActiv"],
                caption: _event["isActiv"]
            }
        );
        this._editSelectableOptionTypesListIsAktiv.push(
            {
                value: '0',
                caption: 'FALSE'
            },
            {
                value: '1',
                caption: 'TRUE'
            }
        );

        this._editSelectableOptionTypesListMioEventName.push(
            {
                value: _event["mioEvent"],
                caption: _event["mioEvent"]
            }
        );


        this._editPickedValueEventStatus = ' ';
        this._editPickedValueIntegration = ' ';
        this._editPickedValueEvent = ' ';
        this.editMarketingAutomation = ' ';
        this._editPickedValueMioEventName = ' ';
        this._editPickedValueIsAktiv = false;

    }

    public saveEdit() {
        let itemOrderEvent: BasketEventInterface = {
            id: this._idEvent,
            statusId: this._editPickedValueEventStatus,
            event: this._editEventName,
            mioEvent: this._editPickedValueMioEventName,
            integration: this._editPickedValueIntegration,
            kontaktereignis: this._editPickedValueEvent,
            kontaktereignisID: 0,
            marketingAutomation: this.editMarketingAutomation,
            isActiv: this._editPickedValueIsAktiv
        };

        let dataEventEdit = {
            orderevent: itemOrderEvent
        };

        this.BasketEventService.saveOrderEventEdit(dataEventEdit, this._idEvent).subscribe(
            response => {
                this._pluginTerraBasicConfig.pluginTerraBasicComponent.callStatusEvent('Einstellungen gespeichert.',
                    'success');
                this.isLoading = true;
            },
            (error) => {
                this._pluginTerraBasicConfig.pluginTerraBasicComponent.callStatusEvent('Einstellungen nicht gespeichert.' + ': ' + error.statusText,
                    'danger');
                this.isLoading = false;
            }
        );
    }
}
