export interface SelectBoxValueInterface
{
    value:any;
    caption:string | number;
    icon?:string;
    position?:number;
}
