export interface BasketEventInterface
{
    id:number;
    statusId: number;
    event: string;
    mioEvent:string;
    integration: string;
    kontaktereignis: string;
    kontaktereignisID: number;
    marketingAutomation: string;
    isActiv: boolean;
}