import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import {
    TerraBaseService,
    TerraLoadingSpinnerService
} from '@plentymarkets/terra-components';
import { Observable } from 'rxjs';


@Injectable()

export class BasketEventService extends TerraBaseService
{

    constructor(private _loadingSpinnerService:TerraLoadingSpinnerService,
                private _http:Http)
    {
        super(_loadingSpinnerService, _http, '/rest/');
        console.log(this.url);
    }

    public getAllOrderEvent():Observable<any>
    {


        this.setAuthorization();
        let url:string;

        url = this.url + 'mailInOne/getAllOrderEvent/';

        return this.mapRequest
        (
            this.http.get(url, {
                headers: this.headers,
                body:    ''
            })
        );
    }

    public getAllStatusOrder():Observable<any>
    {


        this.setAuthorization();
        let url:string;

        url = this.url + 'mailInOne/getAllStatusOrder/';

        return this.mapRequest
        (
            this.http.get(url, {
                headers: this.headers,
                body:    ''
            })
        );
    }

    public saveOrderEvent(data:any):Observable<any>
    {
        this.setAuthorization();

        let url:string;

        url = this.url + 'mailInOne/saveOrderEvent/';

        return this.mapRequest(
            this.http.post(url, data, {headers: this.headers})
        );
    }

    public saveOrderEventEdit(data:any, id:number):Observable<any>
    {
        this.setAuthorization();

        let url:string;
        url = this.url + 'mailInOne/saveOrderEventEdit/';
        return this.mapRequest(
            this.http.put(url, data, {headers: this.headers})
        );
    }
    public deleteOrderEvent(data:any):Observable<any>
    {
        this.setAuthorization();

        let url:string;

        url = this.url + 'mailInOne/deleteOrderEvent/';

        return this.mapRequest(
            this.http.post(url, data, {headers: this.headers})
        );
    }
}
