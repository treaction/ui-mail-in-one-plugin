import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import {
    TerraBaseService,
    TerraLoadingSpinnerService
} from '@plentymarkets/terra-components';
import { Observable } from 'rxjs';


@Injectable()

export class EventService extends TerraBaseService
{

    constructor(private _loadingSpinnerService:TerraLoadingSpinnerService,
                private _http:Http)
    {
        super(_loadingSpinnerService, _http, '/rest/');
        console.log(this.url);
    }

    public getEvent():Observable<any>
    {


        this.setAuthorization();
        let url:string;

        url = this.url + 'mailInOne/getEvent/';

        return this.mapRequest
        (
            this.http.get(url, {
                headers: this.headers,
                body:    ''
            })
        );
    }



    public saveEvent(data:any):Observable<any>
    {
        this.setAuthorization();

        let url:string;

        url = this.url + 'mailInOne/saveEvent/';

        return this.mapRequest(
            this.http.post(url, data, {headers: this.headers})
        );
    }

    public saveEventEdit(data:any, id:number):Observable<any>
    {
        this.setAuthorization();

        let url:string;
        url = this.url + 'mailInOne/saveEventEdit/';
        return this.mapRequest(
            this.http.put(url, data, {headers: this.headers})
        );
    }
    public deleteEvent(data:any):Observable<any>
    {
        this.setAuthorization();

        let url:string;

        url = this.url + 'mailInOne/deleteEvent/';

        return this.mapRequest(
            this.http.post(url, data, {headers: this.headers})
        );
    }
}
