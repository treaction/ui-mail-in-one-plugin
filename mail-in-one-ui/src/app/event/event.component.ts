import {
    Component,
    OnInit
} from '@angular/core';
import {
    Translation,
    TranslationService,
    Language
} from 'angular-l10n';

import { EventService } from "./service/event.service";
import { TerraSimpleTableHeaderCellInterface, TerraSimpleTableRowInterface } from "@plentymarkets/terra-components";
import { PluginTerraBasicConfig } from '../mail-in-one.config';
import { EventInterface } from "./data/event.interface";
import { SelectBoxValueInterface } from "./data/select-box.interface";
import { ButtonInterface } from "./data/button.interface";

@Component({
    selector: 'event',
    template: require('./event.component.html')
})

export class EventComponent implements OnInit {
    @Language()
    protected lang: string;
    private isLoading: boolean = true;
    private new: boolean = false;
    private edit: boolean = false;
    private id: number;
    private event: string;
    private doiName: string;
    private ordner: string;
    private integration: string;
    private kontaktereignis: string;
    private kontaktereignisID: number;
    private marketingAutomation: string;
    private doiKey: string;
    private isDoi: boolean = false;
    private _selectableOptionTypesListOrdner: Array<SelectBoxValueInterface> = [];
    private _pickedValueOrdner: string;
    private _selectableOptionTypesListContactEvent: Array<SelectBoxValueInterface> = [];
    private _pickedValueContactEvent: string;
    private _selectableOptionTypesListEvent: Array<SelectBoxValueInterface> = [];
    private _pickedValueEvent: string;
    private _selectableOptionTypesListIntegration: Array<SelectBoxValueInterface> = [];
    private _pickedValueIntegration: string;
    private _selectableOptionTypesListDOI: Array<SelectBoxValueInterface> = [];
    private _selectableOptionTypesListDOIMailing: Array<SelectBoxValueInterface> = [];
    private _pickedValueDOIMailing: string;
    private _pickedValueDOI: boolean;

    private _idEvent: number;
    private editMarketingAutomation: string;
    private _editSelectableOptionTypesListOrdner: Array<SelectBoxValueInterface> = [];
    private _editPickedValueOrdner: string;
    private _editSelectableOptionTypesListContactEvent: Array<SelectBoxValueInterface> = [];
    private _editPickedValueContactEvent: string;
    private _editSelectableOptionTypesListEvent: Array<SelectBoxValueInterface> = [];
    private _editPickedValueEvent: string;
    private _editSelectableOptionTypesListIntegration: Array<SelectBoxValueInterface> = [];
    private _editPickedValueIntegration: string;
    private _editSelectableOptionTypesListDOI: Array<SelectBoxValueInterface> = [];
    private _editSelectableOptionTypesListDOIMailing: Array<SelectBoxValueInterface> = [];
    private _editPickedValueDOIMailing: string;
    private _editPickedValueDOI: boolean;


    private headerList: Array<TerraSimpleTableHeaderCellInterface> = [];
    private rowList: Array<TerraSimpleTableRowInterface<any>> = [];

    constructor(public translation: TranslationService,
                private EventService: EventService,
                private _pluginTerraBasicConfig: PluginTerraBasicConfig) {

    }

    ngOnInit() {

        this.headerList = [
            {
                width: '20px',
                caption: 'ID'
            },
            {
                width: '20px',
                caption: this.translation.translate('event')
            },
            {
                width: '20px',
                caption: 'Ordner'
            },
            {
                width: '20px',
                caption: 'Integration'
            },
            {
                width: '20px',
                caption: 'Kontaktereignis'
            },
            {
                width: '20px',
                caption: 'Marketing Automation'
            },
            {
                width: '20px',
                caption: 'DOI Mailing'
            },
            {
                width: '20px',
                caption: 'DOI erforderlich'
            },
            {
                width: '20px',
                caption: 'Löschen'
            },
            {
                width: '20px',
                caption: 'Edit'
            }
        ];

        this.EventService.getEvent().subscribe(
            response => {
                for (let resDB of response['DB']) {
                    if (resDB["isDoi"] == 1) {
                        this.isDoi = true;
                    } else {
                        this.isDoi = false;
                    }
                    let buttonList: Array<ButtonInterface> = [];
                    buttonList.push({
                        icon: "'icon-delete'",
                        caption: 'löschen',
                        clickFunction: (): void => {
                            this.delete(resDB["id"]);
                        }
                    });
                    let buttonEdit: Array<ButtonInterface> = [];
                    buttonEdit.push({
                        icon: "'icon-edit'",
                        caption: 'Edit',
                        clickFunction: (): void => {
                            this.update(resDB, response);
                        }
                    });
                    this.rowList.push(
                        {
                            cellList: [

                                {
                                    caption: resDB["id"]
                                },
                                {
                                    caption: resDB["event"]
                                },
                                {
                                    caption: resDB["ordner"]
                                },
                                {
                                    caption: resDB["integration"]
                                },
                                {
                                    caption: resDB["kontaktereignis"]
                                },
                                {
                                    caption: resDB["marketingAutomation"]
                                },
                                {
                                    caption: resDB["doiName"]
                                },
                                {
                                    caption: this.isDoi
                                },
                                {
                                    buttonList: buttonList
                                },
                                {
                                    buttonList: buttonEdit
                                }

                            ]
                        }
                    );
                }
                for (let resFolders of response['folders']) {
                    this._selectableOptionTypesListOrdner.push(
                        {
                            value: '',
                            caption: ''
                        },
                        {
                            value: resFolders["name"],
                            caption: resFolders["name"]
                        }
                    );
                }
                for (let resConatctEvent of response['conatctEvent']) {
                    this._selectableOptionTypesListEvent.push(
                        {
                            value: '',
                            caption: ''
                        },
                        {
                            value: resConatctEvent["id"][0],
                            caption: resConatctEvent["name"]
                        }
                    );
                }

                for (let resDoiMailings of response['doiMailings']) {
                    this._selectableOptionTypesListDOIMailing.push(
                        {
                            value: '',
                            caption: ''
                        },
                        {
                            value: resDoiMailings["doiKey"],
                            caption: resDoiMailings["name"]
                        }
                    );
                }
            },
            (error) => {
                this._pluginTerraBasicConfig.pluginTerraBasicComponent.callStatusEvent('Ein Fehler ist aufgetreten' + ': ' + error.statusText,
                    'danger');
                this.isLoading = false;
            }
        );


        this._selectableOptionTypesListIntegration.push(
            {
                value: '',
                caption: ''
            },
            {
                value: 'Kontaktereignis',
                caption: 'Kontaktereignis'
            },
            {
                value: 'Marketing Program',
                caption: 'Marketing Program'
            },
            {
                value: 'DOI senden',
                caption: 'DOI senden'
            }
        );

        this._selectableOptionTypesListContactEvent.push(
            {
                value: '',
                caption: ''
            },
            {
                value: 'Neuer Kontakt',
                caption: 'Neuer Kontakt'
            },
            {
                value: 'aktualisierte Kontakt',
                caption: 'aktualisierte Kontakt'
            }
        );

        this._selectableOptionTypesListDOI.push(
            {
                value: '',
                caption: ''
            },
            {
                value: '0',
                caption: 'FALSE'
            },
            {
                value: '1',
                caption: 'TRUE'
            }
        );
    }


    public refreshList() {

        this.rowList = [];
        this.isLoading = true;
        this._pluginTerraBasicConfig.pluginTerraBasicComponent.callStatusEvent('Aktualisiere ...', 'success');

        this.EventService.getEvent().subscribe(
            response => {
                for (let resDB of response['DB']) {
                    if (resDB["isDoi"] == 1) {
                        this.isDoi = true;
                    } else {
                        this.isDoi = false;
                    }
                    let buttonList: Array<ButtonInterface> = [];
                    buttonList.push({
                        icon: "'icon-delete'",
                        caption: 'löschen',
                        clickFunction: (): void => {
                            this.delete(resDB["id"]);
                        }
                    });
                    let buttonEdit: Array<ButtonInterface> = [];
                    buttonEdit.push({
                        icon: "'icon-edit'",
                        caption: 'Edit',
                        clickFunction: (): void => {
                            this.update(resDB, response);
                        }
                    });
                    this.rowList.push(
                        {
                            cellList: [

                                {
                                    caption: resDB["id"]
                                },
                                {
                                    caption: resDB["event"]
                                },
                                {
                                    caption: resDB["ordner"]
                                },
                                {
                                    caption: resDB["integration"]
                                },
                                {
                                    caption: resDB["kontaktereignis"]
                                },
                                {
                                    caption: resDB["marketingAutomation"]
                                },
                                {
                                    caption: resDB["doiName"]
                                },
                                {
                                    caption: this.isDoi
                                },
                                {
                                    buttonList: buttonList
                                },
                                {
                                    buttonList: buttonEdit
                                }

                            ]
                        }
                    );
                }
            },
            (error) => {
                this._pluginTerraBasicConfig.pluginTerraBasicComponent.callStatusEvent('Ein Fehler ist aufgetreten' + ': ' + error.statusText,
                    'danger');
                this.isLoading = false;
            }
        );
        this.isLoading = false;
    }


    public add() {
        this.new = true;
        this.edit = false;
    }

    public return() {
        this.new = false;
        this.edit = false;
    }

    public update(_event, _response) {

        this.edit = true;
        this.new = false;


        this._idEvent = _event["id"];

        this._editSelectableOptionTypesListOrdner = [];
        this._editSelectableOptionTypesListContactEvent = [];
        this._editSelectableOptionTypesListEvent = [];
        this._editSelectableOptionTypesListIntegration = [];
        this._editSelectableOptionTypesListDOIMailing = [];
        this._editSelectableOptionTypesListDOI = [];

        this._editSelectableOptionTypesListContactEvent.push(
            {
                value: _event["event"],
                caption: _event["event"]
            }
        );
        this._editSelectableOptionTypesListContactEvent.push(
            {
                value: 'Neuer Kontakt',
                caption: 'Neuer Kontakt'
            },
            {
                value: 'aktualisierte Kontakt',
                caption: 'aktualisierte Kontakt'
            }
        );


        this._editSelectableOptionTypesListOrdner.push(
            {
                value: _event["ordner"],
                caption: _event["ordner"]
            }
        );
        for (let resFolders of _response['folders']) {
            this._editSelectableOptionTypesListOrdner.push(
                {
                    value: resFolders["name"],
                    caption: resFolders["name"]
                }
            );
        }


        this._editSelectableOptionTypesListIntegration.push(
            {
                value: _event["integration"],
                caption: _event["integration"]
            }
        );
        this._editSelectableOptionTypesListIntegration.push(
            {
                value: 'Kontaktereignis',
                caption: 'Kontaktereignis'
            },
            {
                value: 'Marketing Program',
                caption: 'Marketing Program'
            },
            {
                value: 'DOI senden',
                caption: 'DOI senden'
            }
        );


        this._editSelectableOptionTypesListEvent.push(
            {
                value: _event["kontaktereignis"],
                caption: _event["kontaktereignis"]
            }
        );
        for (let resConatctEvent of _response['conatctEvent']) {

            this._editSelectableOptionTypesListEvent.push(
                {
                    value: resConatctEvent["id"][0],
                    caption: resConatctEvent["name"]
                }
            );
        }


        this._editSelectableOptionTypesListDOIMailing.push(
            {
                value: _event["doiKey"],
                caption: _event["doiName"]
            }
        );
        for (let resDoiMailings of _response['doiMailings']) {
            this._editSelectableOptionTypesListDOIMailing.push(
                {
                    value: resDoiMailings["doiKey"],
                    caption: resDoiMailings["name"]
                }
            );
        }

        if (_event["isDoi"] == 1) {
            this.doiName = 'TRUE';
        } else {
            this.doiName = 'FALSE';
        }
        this._editSelectableOptionTypesListDOI.push(
            {
                value: _event["isDoi"],
                caption: this.doiName
            }
        );
        this._editSelectableOptionTypesListDOI.push(
            {
                value: '0',
                caption: 'FALSE'
            },
            {
                value: '1',
                caption: 'TRUE'
            }
        );


        this._editPickedValueContactEvent = ' ';
        this._editPickedValueOrdner = '';
        this._editPickedValueIntegration = ' ';
        this._editPickedValueEvent = ' ';
        this.editMarketingAutomation = ' ';
        this._editPickedValueDOIMailing = ' ';
        this._editPickedValueDOI;

    }

    public save() {
        let itemEvent: EventInterface = {
            id: 0,
            event: this._pickedValueContactEvent,
            ordner: this._pickedValueOrdner,
            integration: this._pickedValueIntegration,
            kontaktereignis: this._pickedValueEvent,
            kontaktereignisID: 0,
            marketingAutomation: this.marketingAutomation,
            doiKey: this._pickedValueDOIMailing,
            isDoi: this._pickedValueDOI
        };

        let dataEvent = {
            event: itemEvent
        };

        this.EventService.saveEvent(dataEvent).subscribe(
            response => {
                this._pluginTerraBasicConfig.pluginTerraBasicComponent.callStatusEvent('Einstellungen gespeichert.',
                    'success');
                this.isLoading = true;
            },
            (error) => {
                this._pluginTerraBasicConfig.pluginTerraBasicComponent.callStatusEvent('Einstellungen nicht gespeichert.' + ': ' + error.statusText,
                    'danger');
                this.isLoading = false;
            }
        );
    }

    public saveEdit() {
        let itemEvent: EventInterface = {

            id: this._idEvent,
            event: this._editPickedValueContactEvent,
            ordner: this._editPickedValueOrdner,
            integration: this._editPickedValueIntegration,
            kontaktereignis: this._editPickedValueEvent,
            kontaktereignisID: 0,
            marketingAutomation: this.editMarketingAutomation,
            doiKey: this._editPickedValueDOIMailing,
            isDoi: this._editPickedValueDOI
        };

        let dataEventEdit = {
            event: itemEvent
        };

        this.EventService.saveEventEdit(dataEventEdit, this._idEvent).subscribe(
            response => {
                this._pluginTerraBasicConfig.pluginTerraBasicComponent.callStatusEvent('Einstellungen gespeichert.',
                    'success');
                this.isLoading = true;
            },
            (error) => {
                this._pluginTerraBasicConfig.pluginTerraBasicComponent.callStatusEvent('Einstellungen nicht gespeichert.' + ': ' + error.statusText,
                    'danger');
                this.isLoading = false;
            }
        );
    }


    public delete(_id) {

        let dataId = {
            eventId: _id
        };

        this.EventService.deleteEvent(dataId).subscribe(
            response => {
                this._pluginTerraBasicConfig.pluginTerraBasicComponent.callStatusEvent('Einstellungen gespeichert.',
                    'success');
                this.isLoading = true;
            },
            (error) => {
                this._pluginTerraBasicConfig.pluginTerraBasicComponent.callStatusEvent('Ein Fehler ist aufgetreten' + ': ' + error.statusText,
                    'danger');
                this.isLoading = false;
            }
        );
    }
}
