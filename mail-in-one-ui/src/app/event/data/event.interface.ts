export interface EventInterface
{
    id:number;
    event: string;
    ordner: string;
    integration: string;
    kontaktereignis: string;
    kontaktereignisID: number;
    marketingAutomation: string;
    doiKey:string;
    isDoi: boolean;
}