import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {
    TerraBaseService,
    TerraLoadingSpinnerService
} from '@plentymarkets/terra-components';
import {Observable} from 'rxjs';


@Injectable()

export class ContactSyncService extends TerraBaseService {

    constructor(private _loadingSpinnerService: TerraLoadingSpinnerService,
                private _http: Http) {
        super(_loadingSpinnerService, _http, '/rest/');
        console.log(this.url);
    }

    public getSyncInfo(): Observable<any> {


        this.setAuthorization();
        let url: string;

        url = this.url + 'mailInOne/getSyncInfo/';

        return this.mapRequest
        (
            this.http.get(url, {
                headers: this.headers,
                body: ''
            })
        );
    }

    public sync(data: any): Observable<any> {


        this.setAuthorization();
        let url: string;

        url = this.url + 'mailInOne/setSync/';

        return this.mapRequest(
            this.http.post(url, data, {headers: this.headers})
        );
    }

    public setEvent(dataEvent: any): Observable<any> {


        this.setAuthorization();
        let url: string;

        url = this.url + 'mailInOne/setEvent/';

        return this.mapRequest(
            this.http.post(url, dataEvent, {headers: this.headers})
        );
    }

    public syncAllContacts(dataReset: any): Observable<any> {
        this.setAuthorization();
        let url: string;

        url = this.url + 'mailInOne/syncAllContacts/';

        return this.mapRequest(
            this.http.post(url, dataReset, {headers: this.headers})
        );
    }

    // WEB-3742 V3.1.1
    // $apiRouter->get('mailInOne/uiGetCurrentJobStatus/', 'SettingsController@uiGetCurrentJobStatus');
    // returns  'jobactive' or 'jobinactive'
    public uiGetCurrentJobStatus(): Observable<any> {
        this.setAuthorization();
        let url: string;

        url = this.url + 'mailInOne/uiGetCurrentJobStatus/';

        return this.mapRequest(
            this.http.get(url, {
                headers: this.headers,
                body: ''
            }));
    }


    // WEB-3742 V3.1.1: "2) Manueller Start der Delta-Synchronisation"
    // $apiRouter->get('mailInOne/uiStartDeltaSync/', 'SyncController@uiStartDeltaSync');
    public uiStartDeltaSync(): Observable<any> {
        this.setAuthorization();
        let url: string;

        url = this.url + 'mailInOne/uiStartDeltaSync/';

        return this.mapRequest(
            this.http.get(url, {
                headers: this.headers,
                body: ''
            }));
    }

}
