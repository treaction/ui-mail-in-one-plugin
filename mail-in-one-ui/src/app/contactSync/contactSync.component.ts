import {
    Component,
    OnInit
} from '@angular/core';
import {
    Translation,
    TranslationService
} from 'angular-l10n';

import { ContactSyncService } from "./service/contactSync.service";
import { TerraSimpleTableHeaderCellInterface, TerraSimpleTableRowInterface } from "@plentymarkets/terra-components";
import { PluginTerraBasicConfig } from '../mail-in-one.config';
import { SettingsService } from "../settings/service/settings.service";
import { AccountInterface } from "../settings/data/account.interface";


@Component({
    selector: 'contactSync',
    template: require('./contactSync.component.html')
})

export class ContactSyncComponent implements OnInit {
    private service: ContactSyncService;
    private isLoading: boolean = true;
    private headerList: Array<TerraSimpleTableHeaderCellInterface> = [];
    private rowList: Array<TerraSimpleTableRowInterface<any>> = [];
    private jobStatus: string ='';
    private isJobActive: boolean=false;
    private deltaSyncStatus: string = '';
    private hasDeltaSyncStatus: boolean = false;

    constructor(
        public translation: TranslationService,
        private contactSyncService: ContactSyncService,
        private _pluginTerraBasicConfig: PluginTerraBasicConfig) {
        this.service = contactSyncService;
    }

    ngOnInit() {
        this.jobStatus = '';
        this.isJobActive = false;
        this.uiGetCurrentJobStatus();


        this.headerList = [
            {
                width: '20px',
                caption: 'letzte Synchronisierung am'
            },
            {
                width: '20px',
                caption: 'synchronisierte Kontakte'
            },
            {
                width: '20px',
                caption: 'ausgelöste Ereignisse'
            },
            {
                width: '20px',
                caption: 'gestartete Marketing Automation'
            },
            {
                width: '20px',
                caption: 'Ausgelöste DOI'
            }
        ]

        this.service.getSyncInfo().subscribe(
            response => {
                for (let resLog of response['log']) {
                    this.rowList.push(
                        {
                            cellList: [

                                {
                                    caption: resLog["date"]
                                },
                                {
                                    caption: resLog["countContact"]
                                },
                                {
                                    caption: resLog["event"]
                                },
                                {
                                    caption: resLog["marketingAutomation"]
                                },
                                {
                                    caption: resLog["doi"]
                                },
                            ]
                        }
                    )
                }
            },
            (error) => {
                this._pluginTerraBasicConfig.pluginTerraBasicComponent.callStatusEvent('Ein Fehler ist aufgetreten' + ': ' + error.statusText,
                    'danger');
                this.isLoading = false;
            }
        );
    }


    public refresh() {
        this.rowList = [];
        this.isLoading = true;
        this._pluginTerraBasicConfig.pluginTerraBasicComponent.callStatusEvent('Aktualisiere ...',
            'success');

        // job status
        this.jobStatus = '';
        this.isJobActive = false;
        this.uiGetCurrentJobStatus();

        this.service.getSyncInfo().subscribe(
            response => {
                for (let resLog of response['log']) {
                    this.rowList.push(
                        {
                            cellList: [

                                {
                                    caption: resLog["date"]
                                },
                                {
                                    caption: resLog["countContact"]
                                },
                                {
                                    caption: resLog["event"]
                                },
                                {
                                    caption: resLog["marketingAutomation"]
                                },
                                {
                                    caption: resLog["doi"]
                                },
                            ]
                        }
                    )
                }
            },
            (error) => {
                this._pluginTerraBasicConfig.pluginTerraBasicComponent.callStatusEvent('Ein Fehler ist aufgetreten' + ': ' + error.statusText,
                    'danger');
                this.isLoading = false;
            }
        );
        this.isLoading = false;
        let temp = (this.isJobActive) ? 'true': 'false';
        console.log('in refresh: jobstatus='  + this.jobStatus+', isJobActive='+temp);

    }

    public sync() {
        let item = {

        };
        let data = {
            info: item
        };

        this.uiGetCurrentJobStatus();
        if(this.isJobActive) {
            this._pluginTerraBasicConfig.pluginTerraBasicComponent.callStatusEvent('Momentan läuft ein Job. Bitte warten.',
                'success');
            return true;
        }

        this._pluginTerraBasicConfig.pluginTerraBasicComponent.callStatusEvent('Die Synchronisation ist gestartet',
            'success');
        this.isJobActive = true;

        this.service.sync(data).subscribe(
            response => {
                this._pluginTerraBasicConfig.pluginTerraBasicComponent.callStatusEvent('Die Synchronisation ist beendet',
                    'success');
                this.isLoading = true;
                this.sleep(2);
                this.uiGetCurrentJobStatus();
            },
            (error) => {
                this._pluginTerraBasicConfig.pluginTerraBasicComponent.callStatusEvent('Ein Fehler ist aufgetreten' + ': ' + error.statusText,
                    'danger');
                this.isLoading = false;
                this.sleep(2);
                this.uiGetCurrentJobStatus();
            }
        );

    }

    public syncAllContacts() {
        let item = {
        };
        let dataReset = {
            updateInfo: item
        };

        this.uiGetCurrentJobStatus();
        if(this.isJobActive) {
            this._pluginTerraBasicConfig.pluginTerraBasicComponent.callStatusEvent('Momentan läuft ein Job. Bitte warten.',
                'success');
            return true;
        }

        this._pluginTerraBasicConfig.pluginTerraBasicComponent.callStatusEvent('Die Synchronisation ist gestartet',
            'success');
        this.isJobActive = true;

        this.service.syncAllContacts(dataReset).subscribe(
            response => {
                this._pluginTerraBasicConfig.pluginTerraBasicComponent.callStatusEvent('Die Synchronisation ist beendet',
                    'success');
                this.isLoading = true;
                this.sleep(2);
                this.uiGetCurrentJobStatus();
            },
            (error) => {
                this._pluginTerraBasicConfig.pluginTerraBasicComponent.callStatusEvent('Ein Fehler ist aufgetreten' + ': ' + error.statusText,
                    'danger');
                this.isLoading = false;
                this.sleep(2);
                this.uiGetCurrentJobStatus();
            }
        );
    }

    public setEvent() {
        let item = {

        };
        let dataEvent = {
            eventInfo: item
        };

        this.uiGetCurrentJobStatus();
        if(this.isJobActive) {
            this._pluginTerraBasicConfig.pluginTerraBasicComponent.callStatusEvent('Momentan läuft ein Job. Bitte warten.',
                'success');
            return true;
        }

        this._pluginTerraBasicConfig.pluginTerraBasicComponent.callStatusEvent('Die Synchronisation ist gestartet',
            'success');
        this.isJobActive = true;

        this.service.setEvent(dataEvent).subscribe(
            response => {
                this._pluginTerraBasicConfig.pluginTerraBasicComponent.callStatusEvent('Die Synchronisation ist beendet',
                    'success');
                this.isLoading = true;
                this.sleep(2);
                this.uiGetCurrentJobStatus();

            },
            (error) => {
                this._pluginTerraBasicConfig.pluginTerraBasicComponent.callStatusEvent('Ein Fehler ist aufgetreten' + ': ' + error.statusText,
                    'danger');
                this.isLoading = false;
                this.sleep(2);
                this.uiGetCurrentJobStatus();
            }
        );

    }

    // WEB-3742
    public uiGetCurrentJobStatus() {
        // // returns  'jobactive' or 'jobinactive'
        this.service.uiGetCurrentJobStatus().subscribe(
            response => {
                console.log('reponse='+ response);
                console.log('reponse[status]='+ response['status']);
                this.jobStatus = response['status'];
                this.isJobActive = (this.jobStatus === 'jobactive') ? true : false;
                let temp = (this.isJobActive)?'true':'false';
                console.log('STATUS:'+this.jobStatus+', isJobActive:'+ temp);
            }
        )
    }


    // WEB-3742
    public uiStartDeltaSync() {

        this.uiGetCurrentJobStatus();
        if(this.isJobActive) {
            this._pluginTerraBasicConfig.pluginTerraBasicComponent.callStatusEvent('Momentan läuft ein Job. Bitte warten.',
                'success');
            return true;
        } else {
            this.isLoading = true;
            this.isJobActive = true;
            this._pluginTerraBasicConfig.pluginTerraBasicComponent.callStatusEvent('Die Synchronisation ist gestartet',
                'success');
        }

        this.service.uiStartDeltaSync().subscribe(
            response => {
                this.deltaSyncStatus = response;

                if(this.deltaSyncStatus !== '') {
                    this.hasDeltaSyncStatus = true;
                }
                this.isLoading = false;
                this.sleep(2);
                this.uiGetCurrentJobStatus();
                this._pluginTerraBasicConfig.pluginTerraBasicComponent.callStatusEvent('Die Synchronisation ist beendet',
                    'success');
            },
           (error) => {
               this.isLoading = false;
               this.isJobActive = false;
               this.sleep(2);
               this.uiGetCurrentJobStatus();
               this._pluginTerraBasicConfig.pluginTerraBasicComponent.callStatusEvent('Ein Fehler ist aufgetreten' + ': ' + error.statusText,
                   'danger');
           }
        )
        this.isJobActive = false;
    }

    private sleep (seconds) {
        var ms = seconds * 1000;
        return new Promise(resolve => setTimeout(resolve, ms));
    }
}
