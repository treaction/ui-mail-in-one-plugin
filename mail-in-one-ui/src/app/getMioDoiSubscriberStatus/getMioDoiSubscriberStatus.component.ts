import {
    Component,
    Input,
    OnInit
} from '@angular/core';
import {
    Translation,
    TranslationService
} from 'angular-l10n';

import { getMioDoiSubscriberStatusService } from "./service/getMioDoiSubscriberStatus.service";
import { TerraSimpleTableHeaderCellInterface, TerraSimpleTableRowInterface } from "@plentymarkets/terra-components";
import { PluginTerraBasicConfig } from '../mail-in-one.config';

@Component({
    selector: 'getMioDoiSubscriberStatus',
    template: require('./getMioDoiSubscriberStatus.component.html'),
    styles: [require('./getMioDoiSubscriberStatus.component.scss')],
})

export class GetMioDoiSubscriberStatusComponent implements OnInit {

    @Input() myTitle: string;
    private service: getMioDoiSubscriberStatusService;
    private isLoading: boolean = true;
    private targetGroup: any;
    private headerList: Array<TerraSimpleTableHeaderCellInterface> = [];
    private rowList: Array<TerraSimpleTableRowInterface<any>> = [];

    constructor(
        public translation: TranslationService,
        private mioDoiSubscriberStatusService: getMioDoiSubscriberStatusService,
        private _pluginTerraBasicConfig: PluginTerraBasicConfig
    ) {
        this.service = mioDoiSubscriberStatusService;
    }

    ngOnInit() {

        this.headerList = [
            {
                width: '20px',
                caption: 'Job-ID (NL)'
            },
            {
                width: '20px',
                caption: 'NL-ID'
            },
            {
                width: '20px',
                caption: 'CRM-ID'
            },
            {
                width: '20px',
                caption: 'NL-Email'
            },
            {
                width: '20px',
                caption: 'NL-Bestellung'
            },
            {
                width: '20px',
                caption: 'NL in Plenty bestätigt'
            },
            {
                width: '20px',
                caption: 'Job-ID (DOI Ok)'
            },
            {
                width: '20px',
                caption: 'MIO Kontakt-ID'
            },
            {
                width: '20px',
                caption: ' MIO DOI-Typ'
            },
            {
                width: '20px',
                caption: 'MIO DOI-Status'
            },
            {
                width: '20px',
                caption: 'MIO DOI bestätigt'
            },
            {
                width: '20px',
                caption: 'MIO Ordner'
            }
        ];

        this.mioDoiSubscriberStatusService.getMioDoiSubscriberStatus().subscribe(
            response => {
                for (let subscrbr_status of response) {

                    console.log('subscrbr_status = ' + JSON.stringify(subscrbr_status));
                    this.rowList.push(
                        {
                            cellList: [
                                {
                                    caption: subscrbr_status["insertByJobId"]
                                },
                                {
                                    caption: subscrbr_status["nlId"]
                                },
                                {
                                    caption: (subscrbr_status["crmId"] == 0) ? '' : subscrbr_status["crmId"]
                                },
                                {
                                    caption: subscrbr_status["nlEmail"]
                                },
                                {
                                    caption: subscrbr_status["timestamp"]
                                },
                                {
                                    caption: subscrbr_status["confirmedTimestamp"]
                                },
                                {
                                    caption: (subscrbr_status["updateByJobId"] == 0) ? '' : subscrbr_status["updateByJobId"]
                                },
                                {
                                    caption: (subscrbr_status["mioId"] == 0) ? '' : subscrbr_status["mioId"]
                                },
                                {
                                    caption: subscrbr_status["mioPermissionType"]
                                },
                                {
                                    caption: subscrbr_status["mioPermissionStatus"]
                                },
                                {
                                    caption: subscrbr_status["mioUpdated"]
                                },
                                {
                                    caption: subscrbr_status["mioCustomFields"]
                                }
                            ]
                        }
                    )
                }
            },
            (error) => {
                this._pluginTerraBasicConfig.pluginTerraBasicComponent.callStatusEvent('Keine Daten vorhanden' + ': ' + error.statusText,
                    'danger');
                this.isLoading = false;
            }
        );
    }

    public refresh() {
        this.rowList = [];
         this._pluginTerraBasicConfig.pluginTerraBasicComponent.callStatusEvent('Aktualisiere ...', 'success');

        this.mioDoiSubscriberStatusService.getMioDoiSubscriberStatus().subscribe(
            response => {
                for (let subscrbr_status of response) {

                    console.log('subscrbr_status = ' + JSON.stringify(subscrbr_status));
                    this.rowList.push(
                        {
                            cellList: [
                                {
                                    caption: subscrbr_status["insertByJobId"]
                                },
                                {
                                    caption: subscrbr_status["nlId"]
                                },
                                {
                                    caption: (subscrbr_status["crmId"] == 0) ? '' : subscrbr_status["crmId"]
                                },
                                {
                                    caption: subscrbr_status["nlEmail"]
                                },
                                {
                                    caption: subscrbr_status["timestamp"]
                                },
                                {
                                    caption: subscrbr_status["confirmedTimestamp"]
                                },
                                {
                                    caption: (subscrbr_status["updateByJobId"] == 0) ? '' : subscrbr_status["updateByJobId"]
                                },
                                {
                                    caption: (subscrbr_status["mioId"] == 0) ? '' : subscrbr_status["mioId"]
                                },
                                {
                                    caption: subscrbr_status["mioPermissionType"]
                                },
                                {
                                    caption: subscrbr_status["mioPermissionStatus"]
                                },
                                {
                                    caption: subscrbr_status["mioUpdated"]
                                },
                                {
                                    caption: subscrbr_status["mioCustomFields"]
                                }
                            ]
                        }
                    )
                }
            },
            (error) => {
                this._pluginTerraBasicConfig.pluginTerraBasicComponent.callStatusEvent('Keine Daten vorhanden' + ': ' + error.statusText,
                    'danger');
                this.isLoading = false;
            }
        );
        this.isLoading = false;
    }
}
