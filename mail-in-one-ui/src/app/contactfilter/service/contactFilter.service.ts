import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import {
    TerraBaseService,
    TerraLoadingSpinnerService
} from '@plentymarkets/terra-components';
import { Observable } from 'rxjs';


@Injectable()

export class ContactFilterService extends TerraBaseService
{

    constructor(private _loadingSpinnerService:TerraLoadingSpinnerService,
                private _http:Http)
    {
        super(_loadingSpinnerService, _http, '/rest/');
        console.log(this.url);
    }


    public getContactFilter():Observable<any>
    {


         this.setAuthorization();
         let url:string;

         url = this.url + 'mailInOne/getContactFilter/';

         return this.mapRequest
         (
             this.http.get(url, {
                 headers: this.headers,
                 body:    ''
             })
         );
    }
}
