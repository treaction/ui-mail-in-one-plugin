import {
    Component,
    OnInit
} from '@angular/core';
import {
    Translation,
    TranslationService
} from 'angular-l10n';

import { ContactFilterService } from "./service/contactFilter.service";
import { TerraSimpleTableHeaderCellInterface, TerraSimpleTableRowInterface } from "@plentymarkets/terra-components";
import { PluginTerraBasicConfig } from '../mail-in-one.config';

@Component({
    selector: 'contactfilter',
    template: require('./contactfilter.component.html'),
    styles: [require('./contactfilter.component.scss')],
})
export class ContactFilterComponent implements OnInit {

    private isLoading: boolean = true;
    private headerList: Array<TerraSimpleTableHeaderCellInterface> = [];
    private rowList: Array<TerraSimpleTableRowInterface<any>> = [];

    constructor(public translation: TranslationService,
                private ContactFilterService: ContactFilterService,
                private _pluginTerraBasicConfig: PluginTerraBasicConfig) {

    }

    ngOnInit() {

        this.headerList = [
            {
                width: '20px',
                caption: 'id'
            },
            {
                width: '20px',
                caption: 'Name der Kontaktfilter'
            },
            {
                width: '20px',
                caption: 'Erstellung'
            },
            {
                width: '20px',
                caption: 'Kontakte'
            }
        ];

        this.ContactFilterService.getContactFilter().subscribe(
            response => {
                let res = response['contactfilter'];
                for (let contactFilter of res) {
                    this.rowList.push(
                        {

                            cellList: [

                                {
                                    caption: contactFilter["id"]
                                },
                                {
                                    caption: contactFilter["name"]
                                },
                                {
                                    caption: contactFilter["created"]
                                },
                                {
                                    caption: contactFilter["count_contacts"]
                                }
                            ]
                        }
                    );
                }
            },
            (error) => {
                this._pluginTerraBasicConfig.pluginTerraBasicComponent.callStatusEvent('Kontakt Filter nicht geladen.' + ': ' + error.statusText,
                    'danger');
                this.isLoading = false;
            }
        );
    }

    public refresh() {
        this.rowList = [];
        this._pluginTerraBasicConfig.pluginTerraBasicComponent.callStatusEvent('Aktualisiere ...', 'success');
        this.ContactFilterService.getContactFilter().subscribe(
            response => {
                let res = response['contactfilter'];
                for (let contactFilter of res) {
                    this.rowList.push(
                        {

                            cellList: [

                                {
                                    caption: contactFilter["id"]
                                },
                                {
                                    caption: contactFilter["name"]
                                },
                                {
                                    caption: contactFilter["created"]
                                },
                                {
                                    caption: contactFilter["count_contacts"]
                                }
                            ]
                        }
                    );
                }
            },
            (error) => {
                this._pluginTerraBasicConfig.pluginTerraBasicComponent.callStatusEvent('Kontakt Filter nicht geladen.' + ': ' + error.statusText,
                    'danger');
                this.isLoading = false;
            }
        );
    }
}
