import {
    Component,
    OnInit
} from '@angular/core';
import {
    Translation,
    TranslationService
} from 'angular-l10n';

import {CampaignService} from "./service/campaign.service";
import {TerraSimpleTableHeaderCellInterface, TerraSimpleTableRowInterface} from "@plentymarkets/terra-components";
import { PluginTerraBasicConfig } from '../mail-in-one.config';

@Component({
               selector: 'campaign',
               template: require('./campaign.component.html')
           })
export class CampaignComponent implements OnInit
{

    private isLoading:boolean = true;
    private headerList:Array<TerraSimpleTableHeaderCellInterface> = [];
    private rowList:Array<TerraSimpleTableRowInterface<any>> = [];
    constructor( public translation:TranslationService,
                 private CampaignService:CampaignService,
                 private _pluginTerraBasicConfig:PluginTerraBasicConfig)
    {

    }

    ngOnInit()
    {

        this.headerList = [
                           {
                width:  '20px',
                caption: 'id'
            },
            {
                width:   '20px',
                caption: 'Name der Kampagne'
            },
            {
                width:  '20px',
                caption: 'Status'
            },
            {
                width:  '20px',
                caption: 'Typ'
            },
            {
                width:   '20px',
                caption: 'versendet'
            },
            {
                width:   '20px',
                caption: 'Zeitpunkt'
            },
            {
                width:   '20px',
                caption: 'Öffner'
            },
            {
                width:  '20px',
                caption: 'Öffnungen'
            },
            {
                width:  '20px',
                caption: 'Öffnungsrate'
            },
            {
                width:   '20px',
                caption: 'Klicker'
            },
            {
                width:   '20px',
                caption: 'Klicks'
            },
            {
                width:   '20px',
                caption: 'Klickrate'
            },
            {
                width:   '20px',
                caption: 'Abmelder'
            },
            {
                width:   '20px',
                caption: 'Hardbounces'
            },
            {
                width:   '20px',
                caption: 'Softbounces'
            }
                       ]

        this.CampaignService.getCampaign().subscribe(
            response => {
                let res =  response['mailing'];
                for (let mailing of res) {
                    let field = mailing["fields"]["field"];
                        this.rowList.push(
                            {
                                cellList: [

                                    {
                                        caption: mailing["id"]
                                    },
                                    {
                                        caption: field[0]["value"]
                                    },
                                    {
                                        caption: field[1]["value"]
                                    },
                                    {
                                        caption: field[2]["value"]
                                    },
                                    {
                                        caption: mailing["versendet"]
                                    },
                                    {
                                        caption: mailing["startDate"]
                                    },
                                    {
                                        caption: mailing["openings"]
                                    },
                                    {
                                        caption: mailing["openings_all"]
                                    },
                                    {
                                        caption: mailing["openings_rate"] +'%'
                                    },
                                    {
                                        caption: mailing["klicks"]
                                    },
                                    {
                                        caption: mailing["klicks_all"]
                                    },
                                    {
                                        caption: mailing["klicks_rate"] + '%'
                                    },
                                    {
                                        caption: mailing["abmelder"]
                                    },
                                    {
                                        caption: mailing["hbounces"]
                                    },
                                    {
                                        caption: mailing["sbounces"]
                                    }
                                ]
                            }
                        )
                }
            },
            (error) => {
                this._pluginTerraBasicConfig.pluginTerraBasicComponent.callStatusEvent('Kampagnen nicht geladen.'+ ': ' + error.statusText,
                    'danger');
                this.isLoading = false;
            }
        );
    }

    public refresh() {

        this.rowList = [];
        this.isLoading = true;
        this._pluginTerraBasicConfig.pluginTerraBasicComponent.callStatusEvent('Aktualisiere ...',
            'success');
        this.CampaignService.getCampaign().subscribe(
            response => {
                let res =  response['mailing'];
                for (let mailing of res) {
                    let field = mailing["fields"]["field"];
                    this.rowList.push(
                        {
                            cellList: [

                                {
                                    caption: mailing["id"]
                                },
                                {
                                    caption: field[0]["value"]
                                },
                                {
                                    caption: field[1]["value"]
                                },
                                {
                                    caption: field[2]["value"]
                                },
                                {
                                    caption: mailing["versendet"]
                                },
                                {
                                    caption: mailing["startDate"]
                                },
                                {
                                    caption: mailing["openings"]
                                },
                                {
                                    caption: mailing["openings_all"]
                                },
                                {
                                    caption: mailing["openings_rate"] +'%'
                                },
                                {
                                    caption: mailing["klicks"]
                                },
                                {
                                    caption: mailing["klicks_all"]
                                },
                                {
                                    caption: mailing["klicks_rate"] + '%'
                                },
                                {
                                    caption: mailing["abmelder"]
                                },
                                {
                                    caption: mailing["hbounces"]
                                },
                                {
                                    caption: mailing["sbounces"]
                                }
                            ]
                        }
                    )
                }
            },
            (error) => {
                this._pluginTerraBasicConfig.pluginTerraBasicComponent.callStatusEvent('Kampagnen nicht geladen.'+ ': ' + error.statusText,
                    'danger');
                this.isLoading = false;
            }
        );
        this.isLoading = false;
    }

}
